<?php

use App\Http\Controllers\ProfileController;
use Illuminate\Foundation\Application;
use Illuminate\Support\Facades\Route;
use Inertia\Inertia;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
})->name('welcome');

Route::get('about',function (){
    return view('about');
})->name('about');

Route::get('/dashboard', function () {
    return view('dashboard');
})->middleware(['auth', 'verified'])->name('dashboard');

Route::middleware('auth')->group(function () {
    Route::get('/profile', [ProfileController::class, 'edit'])->name('profile.edit');
    Route::patch('/profile', [ProfileController::class, 'update'])->name('profile.update');
    Route::delete('/profile', [ProfileController::class, 'destroy'])->name('profile.destroy');
});

require __DIR__.'/client_auth.php';

## Admin Routes
Route::prefix('admin')->group(function (){

    Route::middleware('auth')->group(function () {
        Route::get('/', [\App\Http\Controllers\AdminController::class, 'dashboard'])->name('admin.dashboard');
        Route::get('/operacional', [\App\Http\Controllers\OperacionalController::class, 'navigation'])->name('admin.operacional');
        Route::get('/rental/ajax/details/{rentalId}', [\App\Http\Controllers\OperacionalController::class, 'rentalDetails'])->name('admin.rental');
        Route::post('/rental/ajax/update/{rentalId}', [\App\Http\Controllers\OperacionalController::class, 'rentalUpdate'])->name('admin.rental-update');
    });

    require __DIR__.'/auth.php';

});

## Cliente Routes
Route::prefix('client')->middleware('auth:client')->group(function (){

    // Dashboard
    Route::get('dashboard',[\App\Http\Controllers\ClientDashboardController::class,'dashboard'])->name('client.dashboard');

    // Locações
    Route::get('rentals',[\App\Http\Controllers\RentalsController::class,'rentals'])->name('client.rentals');

    // Locações
    Route::get('rentals/{id}',[\App\Http\Controllers\RentalsController::class,'details'])->name('client.rental');

    // Marketplace
    Route::get('marketplace',[\App\Http\Controllers\MarketplaceController::class,'navigation'])->name('client.marketplace');

    // Detalhes do item - esta rota é o conteúdo interno do Drawer
    Route::get('marketplace/item/{id}',[\App\Http\Controllers\MarketplaceController::class,'ajaxDetailItem'])->name('client.marketplace.item');

    // Cart
    Route::prefix('cart')->middleware('auth')->group(function () {

        // Index
        Route::get('/', [\App\Http\Controllers\CartController::class, 'index'])->name('cart.index');

        // Form - Remove
        Route::delete('/remove/{cartItem}', [\App\Http\Controllers\CartController::class, 'destroy'])->name('cart.destroy');

        // Ajax - Add
        Route::post('/ajax/add', [\App\Http\Controllers\CartController::class, 'store'])->name('cart.add');

        // Ajax Update
        Route::put('/ajax/update', [\App\Http\Controllers\CartController::class, 'ajaxUpdate'])->name('cart.update');

        // Ajax - Get Totals
        Route::get('ajax/total',[\App\Http\Controllers\CartController::class,'ajaxTotal'])->name('cart.ajax.total');

    });

    Route::prefix('checkout')->middleware('auth')->group(function(){

        // Configuração das datas e dias
        Route::get('/', [\App\Http\Controllers\CheckoutController::class, 'index'])->name('checkout.index');

        // Reset em uma data de item do carrinho
        Route::get('clean-item-date/{id}',[\App\Http\Controllers\CheckoutController::class,'cleanItemDate'])->name('checkout.clean-item-date');

        // Atualização do carrinho Data e Dias
        Route::post('/update', [\App\Http\Controllers\CheckoutController::class, 'checkoutUpdate'])->name('checkout.update');

        // Tela para revisão do pedido
        Route::get('/confirmation', [\App\Http\Controllers\CheckoutController::class, 'checkoutAction'])->name('checkout.confirmation');

        // Processamento da locação
        Route::get('/confirmation/accepted', [\App\Http\Controllers\CheckoutController::class, 'checkoutAccepted'])->name('checkout.accepted');

    });

});
