<?php

namespace Database\Factories;

use App\Models\Category;
use App\Models\Item;
use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Item>
 */
class ItemFactory extends Factory
{

    protected $model = Item::class;

    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {

        $category = Category::inRandomOrder()->first();

        return [
            'name' => $this->generateProductName( $category->id ),
            'category_id' => $category->id,
            'thumbnail' => $this->getRandomImage( $category->id ),
            'description' => fake()->realText(250),
            'product_sku' =>  fake()->unique()->numerify('####-##'),
            'daily_rental_price' => fake()->randomFloat(2, 10, 100)
        ];
    }

    protected function getRandomImage( $category_id )
    {

        $images = [
            '1'=>'aluguel-de-notebook-lowcost-1.png',
            '2'=>'aluguel-de-desktop-lowcost-2.png',
            '3'=>'aluguel-de-macbook-lowcost.png',
            '4'=>'aluguel-de-celular-lowcost.png',
            '5'=>'aluguel-de-impressora-lowcost.png'
        ];

        return $images[$category_id];

    }

    protected function generateProductName( $category_id ): string
    {

        $typeMap = [
            1=>'Notebook', 2=>'Desktop', 3=>'MacBook', 4=>'Celular',5=>'Impressora',
        ];

        $brands = [
            'TechNova','PixelPulse', 'NexTech','PrimeCore','GalactoSys','UltraDrive', 'QuantumLeap','MobiTech','NeuroSync','EvoTech'
        ];

        $features = [
            'Pro','Lite','Ultra','Max','Plus','Xtreme','Edge','Mini','Prime','Flex'
        ];

        $brand = $brands[array_rand($brands)];
        $feature = $features[array_rand($features)];
        $category = $typeMap[$category_id];

        return "{$category} {$brand} {$feature}";
    }


}
