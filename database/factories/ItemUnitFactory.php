<?php

namespace Database\Factories;

use App\Models\Item;
use App\Models\ItemUnit;
use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\ItemUnit>
 */
class ItemUnitFactory extends Factory
{
    protected $model = ItemUnit::class;

    public function definition(): array
    {
        return [
            'item_id' => Item::factory(),
            'status' => 'available',
            'serial_number' => $this->faker->unique()->numerify('LC-#########-SP')
        ];
    }
}
