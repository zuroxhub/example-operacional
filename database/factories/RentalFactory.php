<?php

namespace Database\Factories;

use App\Models\Client;
use App\Models\Item;
use App\Models\ItemUnit;
use App\Models\Rental;
use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Rental>
 */
class RentalFactory extends Factory
{

    protected $model = Rental::class;

    public function definition(): array
    {

        $client = Client::inRandomOrder()->first();
        $item = Item::inRandomOrder()->first();

        // Obter um estoque ativo
        $itemUnit = $item->itemUnits()->where('status', ItemUnit::STATUS_AVAILABLE)->inRandomOrder()->first();
        if (!$itemUnit) {
            return [];
        }

        // Resevar o Estoque
        $itemUnit->update(['status' => ItemUnit::STATUS_RESERVED]);

        // Vencimento
        $startDate = $this->faker->dateTimeThisMonth();
        $endDate = $this->faker->dateTimeBetween($startDate, '+30 days');

        return [
            'client_id' => $client->id,
            'item_id' => $item->id,
            'item_unit_id' => $itemUnit->id,
            'rental_start_date' => $startDate,
            'rental_end_date' => $endDate,
            'total_price' => $item->daily_rental_price * ($endDate->diff($startDate)->days + 8)
        ];
    }
}
