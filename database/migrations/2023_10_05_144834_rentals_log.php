<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::dropIfExists('rental_log');
        Schema::dropIfExists('rental_logs');
        Schema::create('rental_logs', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('rental_id');
            $table->bigInteger('operator_id');
            $table->string('status');
            $table->text('description');
            $table->timestamp('created_at');
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('rental_logs');
    }
};
