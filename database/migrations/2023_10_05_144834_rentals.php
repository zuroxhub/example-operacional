<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::dropIfExists('rentals');
        Schema::create('rentals', function (Blueprint $table) {
            $table->id('id')->startingValue(10000);
            $table->bigInteger('client_id');
            $table->bigInteger('item_id');
            $table->bigInteger('item_unit_id');
            $table->string('status')->default('request_new');
            $table->date('rental_start_date');
            $table->date('rental_end_date');
            $table->decimal('total_price', 8, 2);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('rentals');
    }
};
