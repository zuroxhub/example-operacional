<?php

namespace Database\Seeders;

use App\Models\Item;
use App\Models\ItemUnit;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class ItensSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {

        for( $i=0; $i<=50; $i++){
            $item = Item::factory()->create();
            ItemUnit::factory(rand(10,100))->create(['item_id' => $item->id]);
        }

    }
}
