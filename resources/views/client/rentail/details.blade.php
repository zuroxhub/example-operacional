<x-client-app-layout>

    <!-- Main Content Wrapper -->
    <main class=" w-full px-[var(--margin-x)] pb-8">

        <x-layout-client-header menu="rentals"/>

        <div class="pt-5"></div>


        <div class="container">
            <div>
                <a class="text-sm px-3" href="{{ route('client.rentals') }}">Locações</a>
            </div>
            <div class="text-4xl pl-3 pb-3 font-light opacity-50">Locação</div>
        </div>



        <div class="text-sm font-medium text-center text-gray-500 border-b border-gray-200 dark:text-gray-400 dark:border-gray-700">
            <ul class="flex flex-wrap -mb-px container">
                <li class="mr-2">
                    <a href="#" class=" @if( !request()->has('viewMode') or request()->get('viewMode')=='all' ) tab-active @else tab-default @endif">Locação</a>
                </li>

                <li class="mr-2">
                    <a href="#" class="  @if( request()->get('viewMode')=='active' ) tab-active @else tab-default @endif">Suporte</a>
                </li>

            </ul>
        </div>


        <div class="container">

        <div class="grid mt-4 gap-3 grid-cols-4">
            <dl>
                <dt class="uppercase text-xs text-gray-400">Nro da Locação</dt>
                <dd class="text-gray-800">{{ $rental['id'] }}</dd>
            </dl>

            <dl>
                <dt class="uppercase text-xs text-gray-400">Status</dt>
                <dd class="text-gray-800">{{ \App\Models\Rental::getStatusLabels()[$rental['status']] }}</dd>
            </dl>

        </div>


            <div class="card mt-5">
                <div class="card-title">Detalhes</div>
                <div class="card-body">
                    <div class="grid  gap-3 grid-cols-12">
                        <div class="col-span-10">
                            <div class="grid  gap-3 grid-cols-4">
                                <dl>
                                    <dt class="uppercase text-xs text-gray-400">Equipamento</dt>
                                    <dd class="text-gray-800">{{ $rental['item']['name'] }}</dd>
                                </dl>
                                <dl>
                                    <dt class="uppercase text-xs text-gray-400">Código</dt>
                                    <dd class="text-gray-800">{{ $rental['item']['product_sku'] }}</dd>
                                </dl>
                            </div>
                            <div class="mt-3 text-sm">
                                {{ nl2br($rental['item']['description']) }}
                            </div>
                        </div>
                        <div class="col-span-2">
                            <div>
                                <img src="{{ asset('images/'.$rental['item']['thumbnail']) }}">
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="card mt-5">
                <div class="card-title">Registro</div>
                <div class="card-body">
                    <div class="grid  gap-3 grid-cols-4">
                        <dl>
                            <dt class="uppercase text-xs text-gray-400">Serial</dt>
                            <dd class="text-gray-800">{{ $rental['unit']['serial_number'] }}</dd>
                        </dl>
                        <dl>
                            <dt class="uppercase text-xs text-gray-400">Status</dt>
                            <dd class="text-gray-800">{{ $rental['unit']['status'] }}</dd>
                        </dl>
                        <dl>
                            <dt class="uppercase text-xs text-gray-400">Duração</dt>
                            <dd class="text-gray-800">{{ \Carbon\Carbon::parse($rental['rental_start_date'])->diffInDays($rental['rental_end_date']) }} dias </dd>
                        </dl>
                        <dl>
                            <dt class="uppercase text-xs text-gray-400">Vlr. Fatura</dt>
                            <dd class="text-gray-800">R$ {{ \App\Services\General::currencyFormatter($rental['total_price']) }}</dd>
                        </dl>
                    </div>
                </div>
            </div>

            <div class="card mt-5">
                <div class="card-title">Histórico</div>
                <div class="card-body bg-white">
                    @foreach(collect($rental->log)->sortByDesc('id') as $log)
                        <div class="text-xs border-t p-3 hover:bg-gray-100 transition-all">
                            <span class="p-1 text-sm">Operador:  <strong>{{ $log['operator']['name'] }}</strong></span>
                            <div class=" my-1 p-2 inline-block {{ \App\Models\Rental::getStatusStyle()[$log['status']] }}"> {{ \App\Models\Rental::getStatusLabels()[$log['status']] }}</div>
                            @if($log['description']!="N/A")<div class="p-1 font-semibold text-sm"> {{ $log['description'] }}</div>@endif
                            <div class="p-1"> {{ \Carbon\Carbon::parse($log['created_at'])->format('d/m/Y H:i') }}</div>
                        </div>
                    @endforeach
                    <div class="text-xs border-t p-3 hover:bg-gray-100 transition-all">
                        <div class="p-1 font-semibold text-sm"> Criação da locação</div>
                        <div class="p-1"> {{ \Carbon\Carbon::parse($rental['created_at'])->format('d/m/Y H:i') }}</div>
                    </div>
                </div>
            </div>

        </div>

    </main>

</x-client-app-layout>
