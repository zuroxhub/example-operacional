
@foreach($avaliableItems as $category=>$itensAvaliable)

    <div>
        <div class="p-5 text-2xl mt-5 border-b mb-4">{{ $category }}</div>

        <div class="grid grid-cols-4 xl:grid-cols-6 gap-6">

            @foreach($itensAvaliable as $item)
                <div class="card hover:shadow-lg transition-all p-4"
                     onclick="window.marketplaceItem({{ $item['item']['id'] }})"
                     type="button" data-drawer-target="drawer-item" data-drawer-show="drawer-item" aria-controls="drawer-item"
                >
                    <div class="px-2 text-sm font-semibold">{{ $item['item']['name'] }}</div>
                    <div class="px-2 mb-4 opacity-50 text-xs"> SKU:  {{ $item['item']['product_sku']}}</div>
                    <div class="marketplace-image-card">
                        <img src="{{ asset( 'images/' . $item['item']['thumbnail']) }}" class=" p-5 w-full h-max rounded-lg">
                    </div>
                    <div class="text-xs bg-gray-50 text-center rounded  py-3 text-gray-400 px-2">
                        {{ $item['item']->availableUnits() }} em estoque
                    </div>
                </div>
            @endforeach
        </div>

    </div>

@endforeach
