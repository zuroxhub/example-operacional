<div>

    <div class="card mb-4 p-4">
        <div class="px-2 text-sm font-semibold">{{ $item['name'] }}</div>
        <div class="px-2 mb-4 opacity-50 text-xs">{{ $item['product_sku']}}</div>
        <div class="marketplace-image-card">
            <img src="{{ asset( 'images/' . $item['thumbnail']) }}" class=" p-5 w-full h-max rounded-lg">
        </div>
        <div class="text-xs bg-gray-50 mt-2 text-center rounded  py-3 text-gray-400 px-2">
            {{ $item->availableUnits() }} em estoque
        </div>
    </div>


    <div class="mb-4 pb-4 text-sm">
        {{ nl2br($item['description']) }}
    </div>

    <div>
        <div class="uppercase text-xs">diária à partir de </div>
        <div class="font-semibold text-3xl mb-5 text-primary">R$ {{ \App\Services\General::currencyFormatter($item['daily_rental_price']) }}</div>
    </div>


    <!--
        Componente para calcular o valor da diária
    -->
    <div x-data="window.calculator('{{ $item['daily_rental_price'] }}', '{{ $item['id'] }}')">

        <div class="flex items-center">

            <!-- Minus -->
            <button @click="decrement()" class="bg-gray-200 text-gray-600 hover:bg-gray-400 hover:text-white rounded-l-lg px-3 py-2 focus:outline-none focus:ring-2 focus:ring-blue-500 dark:bg-gray-700 dark:text-gray-300 dark:hover:bg-gray-600">
                -
            </button>

            <!-- Quantity -->
            <input type="number" disabled class="bg-gray-50 text-center border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500" x-model="quantity" id="quantity" min="0" value="1">

            <!-- Plus -->
            <button @click="increment()" class="bg-gray-200 text-gray-600 hover:bg-gray-400 hover:text-white rounded-r-lg px-3 py-2 focus:outline-none focus:ring-2 focus:ring-blue-500 dark:bg-gray-700 dark:text-gray-300 dark:hover:bg-gray-600">
                +
            </button>
        </div>
        <div class="py-3 px-2 bg-blue-100 font-bold text-blue-500 rounded-lg my-3 text-center">Total: R$ <span x-text="quantityDisplay"></span></div>
    </div>


    <!--
        Formulário para registrar o item no pedido
    -->
    <form id="addItemForm{{ $item['id'] }}" >
    @csrf
        <input type="hidden" name="item_id" id="itemId" value="{{ $item['id'] }}">
        <input type="hidden" name="quantity" id="quantityInput" value="1">
        <div class="grid gap-4">
            <button
                type="button"
                onclick="window.addToCart( event , {{ $item['id'] }} )"
                class="w-full items-center px-4 py-2 text-sm font-medium text-center text-white bg-blue-700 rounded-lg hover:bg-blue-800 focus:ring-4 focus:ring-blue-300 dark:bg-blue-600 dark:hover:bg-blue-700 focus:outline-none dark:focus:ring-blue-800">
                Adicionar
            </button>
        </div>
    </form>

</div>
