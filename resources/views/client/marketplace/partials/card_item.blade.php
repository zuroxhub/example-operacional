<div class="card hover:shadow-lg transition-all p-4">
    <div class="px-2 text-sm font-semibold">{{ $item['item']['name'] }}</div>
    <div class="px-2 mb-4 opacity-50 text-xs">{{ $item['item']['product_sku']}}</div>
    <div class="marketplace-image-card">
        <img src="{{ asset( 'images/' . $item['item']['thumbnail']) }}" class=" p-5 w-full h-max rounded-lg">
    </div>
</div>
