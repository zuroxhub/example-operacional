

    <div>
        <div class="p-5 text-2xl">{{ $avaliableItems['category']['name'] }}</div>

        <div class="grid grid-cols-4 xl:grid-cols-5 gap-6">

           @foreach($avaliableItems['itens'] as $item)
                <div class="card hover:shadow-lg transition-all p-4"
                     onclick="window.marketplaceItem({{ $item['id'] }})"
                     type="button" data-drawer-target="drawer-item" data-drawer-show="drawer-item" aria-controls="drawer-item">
                    <div class="px-2 text-sm font-semibold">{{ $item['name'] }}</div>
                    <div class="px-2 mb-4 opacity-50 text-xs">{{ $item['product_sku']}}</div>
                    <div class="marketplace-image-card">
                        <img src="{{ asset( 'images/' . $item['thumbnail']) }}" class=" p-5 w-full h-max rounded-lg">
                    </div>

                    <div class="text-sm">
                        <div class="uppercase text-xs">à partir de </div>
                        <div class="font-semibold text-primary">R$ {{ \App\Services\General::currencyFormatter($item['daily_rental_price']) }}</div>
                    </div>
                </div>
            @endforeach
        </div>

    </div>

