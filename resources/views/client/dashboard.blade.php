<x-client-app-layout>

    <!-- Main Content Wrapper -->
    <main class=" w-full px-[var(--margin-x)] pb-8">

        <x-layout-client-header menu="dashboard"/>

        <div class="p-3"> </div>

        <div class="container">
            <div class="text-4xl pl-3 pb-3 font-light opacity-50">Dashboard</div>
        </div>

        <div class="container">

            <div class="p-3 text-gray-500">Resumo dos equipamentos ativos.</div>

            <div class="grid gap-2 my-4 mx-3 grid-cols-2">

                <div class="col-span-2">

                    <div class="grid grid-cols-3 gap-3 ">

                        @foreach($itensActive as $categoryName=>$totalItens)
                            <div class="card justify-between p-5">
                                <div class="flex justify-between space-x-1">
                                    <p class="text-3xl font-semibold text-slate-700 dark:text-navy-100">
                                        {{ $totalItens }}
                                    </p>
                                </div>
                                <p class="mt-1 text-xs">{{ $categoryName }}</p>
                            </div>
                        @endforeach

                    </div>

                </div>
            </div>
        </div>


        <div class="container">
            <div class="text-4xl pl-3 pt-5 pb-3 font-light opacity-50">Perfil</div>
        </div>

        <div class="border-b mb-5"></div>

        <div class="container">


            <div class="card mx-3">
                <div class="grid grid-cols-3">

                    <dl class="p-3">
                        <dt  class="uppercase text-sm text-gray-400">Nome da Empresa</dt>
                        <dd class="fw-bold">{{ $businessClient['name'] }}</dd>
                    </dl>

                    <dl class="p-3">
                        <dt class="uppercase text-sm text-gray-400">E-mai</dt>
                        <dd class="fw-bold">{{ $businessClient['email'] }}</dd>
                    </dl>
                    <dl class="p-3">
                        <dt class="uppercase text-sm text-gray-400">Telefone</dt>
                        <dd class="fw-bold">{{ $businessClient['phone'] }}</dd>
                    </dl>

                    <dl class="p-3 col-span-3">
                        <dt class="uppercase text-sm text-gray-400">Endereço</dt>
                        <dd class="fw-bold">{{ $businessClient['address'] }}</dd>
                    </dl>
{{--

                    <dl class="p-3">
                        <dt>Data de Cadastro</dt>
                        <dd class="fw-bold">{{ \Carbon\Carbon::parse($businessClient['created_at'])->format('d/m/Y') }}</dd>
                    </dl>
--}}

                </div>
            </div>
        </div>

        <div class="container">
            <div class="text-4xl pl-3 pt-5 pb-3 font-light opacity-50"> Sessão</div>
        </div>

        <div class="border-b mb-5"></div>

        <div class="container">


            <a href="{{ route('client.logout') }}"
               class="py-2.5 ms-3 px-5 mr-2 mb-2 text-sm font-medium text-gray-900 focus:outline-none bg-white rounded-lg border border-gray-200 hover:bg-gray-100 hover:text-blue-700 focus:z-10 focus:ring-4 focus:ring-gray-200 dark:focus:ring-gray-700 dark:bg-gray-800 dark:text-gray-400 dark:border-gray-600 dark:hover:text-white dark:hover:bg-gray-700"
               onclick="event.preventDefault();document.getElementById('formlogout').submit();">Sair do sistema</a>
            <form method="POST" id="formlogout" action="{{ route('client.logout') }}">
                @csrf
            </form>

        </div>


    </main>

</x-client-app-layout>
