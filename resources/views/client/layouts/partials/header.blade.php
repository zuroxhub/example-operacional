
@inject('cartService', 'App\Services\CartService')
@php($routeName=Route::currentRouteName())

<nav class="bg-white border-gray-200 dark:bg-gray-900">
    <div class="max-w-screen-xl flex flex-wrap items-center justify-between mx-auto p-4">
        <a
            @if( $routeName=="checkout.index" )
            href="{{ route('client.dashboard') }}"
            @else
            href="/"
            @endif
            class="flex items-center">
            <img src="https://flowbite.com/docs/images/logo.svg" class="h-8 mr-3" alt="Flowbite Logo" />
            <span class="self-center text-2xl font-semibold whitespace-nowrap dark:text-white">{{ config('app.name') }} @ APP</span>
        </a>

        <button data-collapse-toggle="navbar-default" type="button" class="inline-flex items-center p-2 w-10 h-10 justify-center text-sm text-gray-500 rounded-lg md:hidden hover:bg-gray-100 focus:outline-none focus:ring-2 focus:ring-gray-200 dark:text-gray-400 dark:hover:bg-gray-700 dark:focus:ring-gray-600" aria-controls="navbar-default" aria-expanded="false">
            <span class="sr-only">Open main menu</span>
            <svg class="w-5 h-5" aria-hidden="true" xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 17 14">
                <path stroke="currentColor" stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M1 1h15M1 7h15M1 13h15"/>
            </svg>
        </button>

        @if( $routeName=="checkout.index" )
            <div class="hidden w-full md:block md:w-auto text-green-500 text-sm font-bold">
                Você está em um ambiente seguro
            </div>
        @else
            <div class="hidden w-full md:block md:w-auto" id="navbar-default">
                <ul class="font-medium flex flex-col p-4 md:p-0 mt-4 border border-gray-100 rounded-lg bg-gray-50 md:flex-row md:space-x-8 md:mt-0 md:border-0 md:bg-white dark:bg-gray-800 md:dark:bg-gray-900 dark:border-gray-700">
                    <li>
                        <a href="{{ route('client.dashboard') }}" class="@if($routeName=='client.dashboard') nav-active @else nav-default @endif" aria-current="page">Dashboard</a>
                    </li>
                    <li>
                        <a href="{{ route('client.rentals') }}?viewMode=active" class="@if($routeName=='client.rentals') nav-active @else nav-default @endif" aria-current="page">Minhas Locações</a>
                    </li>

                    <li>
                        <a href="{{ route('client.marketplace') }}" class="@if($routeName=='client.marketplace') nav-active @else nav-default @endif" aria-current="page">Marketplace</a>
                    </li>

                    @php($totalCartItens=$cartService->getItemCount())

                    <li>
                        <a href="{{ route('cart.index') }}" class="@if($routeName=='cart.index') nav-active @else nav-default @endif" aria-current="page">
                            <span>Novo Pedido</span>
                            (<span id="totalCart">{{$totalCartItens}}</span>)
                        </a>
                    </li>

                </ul>
            </div>
        @endif
    </div>
</nav>

