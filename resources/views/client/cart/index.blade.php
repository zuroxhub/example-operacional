<x-client-app-layout>

@inject('cartService', 'App\Services\CartService')

<!-- Main Content Wrapper -->
    <main class=" w-full px-[var(--margin-x)] pb-8">

        <x-layout-client-header menu="cart"/>

        <div class="p-3"> </div>


        <div class="container">
            <div class="text-4xl pl-3 pb-3 font-light opacity-50">Novo Pedido</div>
        </div>

        <div class="border-b mb-4 mt-5"></div>




        {{--    <div class="text-sm  text-center text-gray-500 border-b border-gray-200 dark:text-gray-400 dark:border-gray-700">
                <ul class="flex flex-wrap -mb-px container">
                    <li class="mr-2">
                        <a href="{{ route('cart.index') }}" class=" @if( !request()->has('categoryId') or request()->get('categoryId')=='all' ) tab-active @else tab-default @endif">Itens</a>
                    </li>
                </ul>
            </div>--}}
        <div class="container">

            <div class="">


                @if(!count($items))
                    <div class="p-3 text-base">Ainda não há itens no seu pedido</div>

                    <a href="{{ route('client.marketplace') }}" class="pl-3 text-sm text-gray-400">Voltar o marketplace</a>
                @endif
                @if(count($items))

                    <div class="min-h-[70vh]">
                        <table class="w-full text-sm text-left text-gray-500 dark:text-gray-400">

                            <tbody>
                            @foreach($items as $item)
                                <tr class="bg-white dark:bg-gray-800 border-b">
                                    <td class="px-1 py-4 w-[50px]">
                                        <img src="{{ asset( 'images/' . $item['item']['thumbnail']) }}" class=" max-w-[100px] m-auto max-h-[80px] h-max rounded-lg">
                                    </td>
                                    <th scope="row" class="px-6 py-4 font-medium text-gray-900 whitespace-nowrap dark:text-white">

                                        <div class="text-sm mb-1 text-gray-800">
                                            {{ $item['item']['name'] }}
                                        </div>
                                        <div class="text-gray-400">
                                            <dl class="grid grid-cols-4 gap-2 text-xs ">
                                                <dt class=" font-normal">SKU</dt>
                                                <dd class="col-span-3 ">{{ $item['item']['product_sku']}}</dd>
                                            </dl>
                                            <dl class="grid grid-cols-4 gap-2 text-xs ">
                                                <dt class="font-normal">Vlr. Unit</dt>
                                                <dd class="col-span-3 ">R$ {{ \App\Services\General::currencyFormatter( $item['item']['daily_rental_price'] ) }}</dd>
                                            </dl>
                                            <dl class="grid grid-cols-4 gap-2 text-xs ">
                                                <dt class="font-normal">Estoque</dt>
                                                <dd class="col-span-3 ">
                                                    {{ $item['item']->availableUnits() }}</dd>
                                            </dl>
                                        </div>

                                    </th>
                                    <td class="px-6 py-4">

                                        <div class="uppercase text-xs mb-1">Quantidade</div>
                                        <div x-data="window.cartItemManager({{ $item['item']['id'] }}, {{ $item->quantity }})">

                                            <input type="text"
                                                   class="bg-gray-50 border text-center border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500"
                                                   x-model="quantity"
                                                   x-debounce="500"
                                                   @input="updateQuantity">
                                        </div>

                                    </td>
                                    <td class="px-6 py-4 text-end">


                            <form action="{{ route('cart.destroy', $item->id) }}" method="POST">
                                @csrf
                                @method('DELETE')
                                <button type="submit" class="px-3 py-2 text-xs   text-center text-rose-800 bg-rose-50  rounded-lg hover:bg-rose-800 focus:ring-4 focus:outline-none focus:ring-rose-300 dark:bg-rose-600 dark:hover:bg-rose-700 dark:focus:ring-rose-800">X</button>
                            </form>

                                    </td>
                                </tr>

                            @endforeach

                            </tbody>
                        </table>
                    </div>

                    <div class="grid grid-cols-2 gap-5 border-t pt-4">
                        <div>
                            <a href="{{ route('client.marketplace') }}" class="text-gray-900 bg-white hover:bg-gray-100 border border-gray-200 focus:ring-4 focus:outline-none focus:ring-gray-100  rounded-lg text-sm px-5 py-2.5 text-center inline-flex items-center dark:focus:ring-gray-600 dark:bg-gray-800 dark:border-gray-700 dark:text-white dark:hover:bg-gray-700 mr-2 mb-2">
                                Voltar ao marketplace
                            </a>
                        </div>
                        <div class="text-center">
                            <a  href="{{ route('checkout.index') }}"  class="text-white w-full block bg-blue-700 w-full hover:bg-blue-800 focus:ring-4 focus:ring-blue-300  rounded-lg text-sm px-5 py-2.5 mr-2 mb-2 dark:bg-blue-600 dark:hover:bg-blue-700 focus:outline-none dark:focus:ring-blue-800">
                                Continuar
                            </a>
                        </div>
                    </div>
                @endif

            </div>


        </div>


</x-client-app-layout>
