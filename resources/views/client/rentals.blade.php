<x-client-app-layout>

    <main class=" w-full px-[var(--margin-x)] pb-8">

        <x-layout-client-header menu="rentals"/>

        <div class="pt-5"></div>

        <div class="container">
            <div class="text-4xl pl-3 pb-3 font-light opacity-50">Locações</div>
        </div>

        <div class="text-sm font-medium text-center text-gray-500 border-b border-gray-200 dark:text-gray-400 dark:border-gray-700">
            <ul class="flex flex-wrap -mb-px container">
                <li class="mr-2">
                    <a href="{{ route('client.rentals') }}?viewMode=all" class=" @if( !request()->has('viewMode') or request()->get('viewMode')=='all' ) tab-active @else tab-default @endif">Todos</a>
                </li>
                <li class="mr-2">
                    <a href="{{ route('client.rentals') }}?viewMode=processing" class="  @if( request()->get('viewMode')=='processing' ) tab-active @else tab-default @endif">Processamento</a>
                </li>
                <li class="mr-2">
                    <a href="{{ route('client.rentals') }}?viewMode=active" class="  @if( request()->get('viewMode')=='active' ) tab-active @else tab-default @endif">Ativos</a>
                </li>
                <li class="mr-2">
                    <a href="{{ route('client.rentals') }}?viewMode=closed" class="  @if( request()->get('viewMode')=='closed' ) tab-active @else tab-default @endif">Finalizados</a>
                </li>
            </ul>
        </div>


        @if(count($rentals)>=1)
            <div class="container">

                <table class="is-hoverable border my-5  text-xs w-full text-left">
                    <thead>
                    <tr>
                        <th colspan="1" class="p-3 border-b bg-gray-100" >
                            Itens
                        </th>
                        <th colspan="4" class="p-3 border-b bg-gray-100" >
                            Status
                        </th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($rentals as $rental)
                        <tr class="border-y border-transparent hover:bg-gray-100 border-b-slate-200 dark:border-b-navy-500 transition-all" onclick="window.location='{{ route('client.rental',[$rental['id']]) }}'">
                            <td class="whitespace-nowrap px-4 py-3 sm:px-5">

                                <div class="flex items-center space-x-4">
                                    <div class="h-12 w-12">
                                        <img src="{{ asset( 'images/' . $rental['item']['thumbnail']) }}" class="h-full w-full rounded-lg">
                                    </div>
                                    <div>
                                        <p class="font-medium text-slate-600 dark:text-navy-100">
                                            {{ $rental['item']['name'] }}
                                        </p>
                                        <p class="mt-1 text-xs text-slate-400 dark:text-navy-300">
                                            {{ $rental['item']['product_sku'] }}
                                        </p>
                                        <p class="mt-1 text-xs text-slate-400 dark:text-navy-300">
                                            <span>Serial: {{ print_r($rental['unit']['serial_number']) }}</span>
                                        </p>
                                    </div>
                                </div>
                            </td>
                            <td class="whitespace-nowrap px-4 py-3 sm:px-5">

                                <div class="border p-3 rounded {{ \App\Models\Rental::getStatusStyle()[$rental['status']] }} ">{{ \App\Models\Rental::getStatusLabels()[$rental['status']] }}</div>
                            </td>
                            <td class="whitespace-nowrap px-4 py-3 sm:px-5  ">
                                <dl>
                                    <dt class="uppercase text-xs text-gray-400">Duração</dt>
                                    <dd class="font-semibold text-xs">{{ \Carbon\Carbon::parse($rental['rental_start_date'])->diffInDays($rental['rental_end_date']) }} dias </dd>
                                </dl>
                            </td>
                            <td class="whitespace-nowrap px-4 py-3 sm:px-5">
                                <dl>
                                    <dt class="uppercase text-xs text-gray-400">Renovação</dt>
                                    <dd class="font-semibold  text-xs">{{ \Carbon\Carbon::parse($rental['rental_end_date'])->format('d/m/Y') }}</dd>
                                </dl>
                            </td>
                            <td class="whitespace-nowrap px-4 py-3 sm:px-5">
                                <dl>
                                    <dt class="uppercase text-xs text-gray-400">Fatura</dt>
                                    <dd class="font-semibold  text-xs">
                                        R$ {{ \App\Services\General::currencyFormatter($rental['total_price'])}}
                                    </dd>
                                </dl>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>

                <div class="pagination-custom transition-all">
                    {{ $rentals->links() }}
                </div>
            </div>
        @endif

    </main>

</x-client-app-layout>
