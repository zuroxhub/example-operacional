<x-client-app-layout>

@inject('cartService', 'App\Services\CartService')


<!-- Main Content Wrapper -->
    <main class=" w-full px-[var(--margin-x)] pb-8">

        <x-layout-client-header menu="marketplace"/>

        <div class="pt-5"></div>



        <div class="container">
            <div class="grid grid-cols-2">
                <div class="text-4xl pl-3 pb-3 font-light opacity-50">Marketplace</div>

            </div>
        </div>



        <div class="text-sm font-medium text-center text-gray-500 border-b border-gray-200 dark:text-gray-400 dark:border-gray-700">
            <ul class="flex flex-wrap -mb-px container">
                <li class="mr-2">
                    <a href="{{ route('client.marketplace') }}?categoryId=all" class=" @if( !request()->has('categoryId') or request()->get('categoryId')=='all' ) tab-active @else tab-default @endif">Tudo</a>
                </li>
                @foreach($categories as $category)
                    <li class="mr-2">
                        <a href="{{ route('client.marketplace') }}?categoryId={{ $category['id'] }}" class=" @if(request()->get('categoryId')==$category['id'] ) tab-active @else tab-default @endif " aria-current="page">
                            {{ $category['name'] }}
                        </a>
                    </li>
                @endforeach
            </ul>
        </div>

        <div class="container">

            @if( !request()->has('categoryId') or request()->get('categoryId')=='all' )
                @include('client.marketplace.all_itens')
            @elseif( request()->get('categoryId')!='all' )
                @include('client.marketplace.category_itens')
            @endif

        </div>

        <div id="drawer-item" class="fixed top-0 left-0 z-40 h-screen p-4 overflow-y-auto transition-transform -translate-x-full bg-white w-80 dark:bg-gray-800" tabindex="-1" aria-labelledby="drawer-label">
            <h5 id="drawer-label" class="inline-flex items-center mb-4 text-base font-semibold text-gray-500 dark:text-gray-400"><svg class="w-4 h-4 mr-2.5" aria-hidden="true" xmlns="http://www.w3.org/2000/svg" fill="currentColor" viewBox="0 0 20 20">
                    <path d="M10 .5a9.5 9.5 0 1 0 9.5 9.5A9.51 9.51 0 0 0 10 .5ZM9.5 4a1.5 1.5 0 1 1 0 3 1.5 1.5 0 0 1 0-3ZM12 15H8a1 1 0 0 1 0-2h1v-3H8a1 1 0 0 1 0-2h2a1 1 0 0 1 1 1v4h1a1 1 0 0 1 0 2Z"/>
                </svg>Informações</h5>
            <button type="button" data-drawer-hide="drawer-item" aria-controls="drawer-item" class="text-gray-400 bg-transparent hover:bg-gray-200 hover:text-gray-900 rounded-lg text-sm w-8 h-8 absolute top-2.5 right-2.5 inline-flex items-center justify-center dark:hover:bg-gray-600 dark:hover:text-white" >
                <svg class="w-3 h-3" aria-hidden="true" xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 14 14">
                    <path stroke="currentColor" stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="m1 1 6 6m0 0 6 6M7 7l6-6M7 7l-6 6"/>
                </svg>
                <span class="sr-only">Close menu</span>
            </button>
            <div id="conteudo"></div>
        </div>

    </main>
</x-client-app-layout>
