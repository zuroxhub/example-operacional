<x-client-app-layout>

    @inject('cartService', 'App\Services\CartService')
    @php($checkoutAvaliable=true)

    @foreach($items as $item)
        @if(!$item['days'] or !$item['start_date'])
            @php($checkoutAvaliable=false)
        @endif
    @endforeach

    <main class=" w-full px-[var(--margin-x)] pb-8">

        <x-layout-client-header menu="checkout"/>

        <div class="p-3"> </div>

        <div class="container">
            <div class="text-4xl pl-3 pb-3 font-light opacity-50">Sucesso</div>
        </div>

        <div class="border-b mb-4 mt-5"></div>

        <div class="container">

            <div class="">

                    <div class="p-4 mb-4 text-sm text-blue-800 rounded-lg bg-blue-50 dark:bg-gray-800 dark:text-blue-400" role="alert">
                        <span class="font-medium">Seu pedido foi recebido</span> Logo iremos atualizarmos por e-mail.
                    </div>

                <div class="min-h-[70vh]">
                    <div class="relative overflow-x-auto">
                        <form method="post" action="{{ route('checkout.update') }}"
                              x-data="{
                                      debounceSubmit: window.debounce(function() { $refs.submitButton.click() }, 600)
                                  }">
                            @csrf
                            <button type="submit" x-ref="submitButton" class="hidden">Submit</button>

                            <table class="w-full text-sm text-left text-gray-500 dark:text-gray-400">

                                <tbody>
                                @foreach($items as $item)
                                    <tr class="bg-white dark:bg-gray-800 border-b">
                                        <td class="px-1 py-4 w-[50px]">
                                            <img src="{{ asset( 'images/' . $item['item']['thumbnail']) }}" class=" max-w-[100px] m-auto max-h-[80px] h-max rounded-lg">
                                        </td>
                                        <th scope="row" class="px-6 py-4 font-medium text-gray-900 whitespace-nowrap dark:text-white">

                                            <div class="text-sm mb-1 text-gray-800">
                                                {{ $item['item']['name'] }}
                                            </div>
                                            <div class="text-gray-400">
                                                <dl class="grid grid-cols-4 gap-2 text-xs ">
                                                    <dt class=" font-normal">SKU</dt>
                                                    <dd class="col-span-3 ">{{ $item['item']['product_sku']}}</dd>
                                                </dl>
                                                <dl class="grid grid-cols-4 gap-2 text-xs ">
                                                    <dt class="font-normal">Vlr. Unit</dt>
                                                    <dd class="col-span-3 ">R$ {{ \App\Services\General::currencyFormatter( $item['item']['daily_rental_price'] ) }}</dd>
                                                </dl>
                                            </div>
                                        </th>
                                        <td class="px-6 py-4 ">

                                            <div class="uppercase text-xs mb-1">Quantidade</div>
                                            <div class="font-bold text-gray-800 p-1">{{ $item->quantity }} <span class="font-light text-gray-400 text-xs"> unidade(s) </span> </div>
                                        </td>
                                        <td class="px-6 py-4">
                                            <div class="uppercase text-xs mb-1">Dias em locação</div>
                                            <input type="number"
                                                   name="days[{{ $item['id'] }}]"
                                                   value="{{ $item['days'] }}"
                                                   x-on:input="debounceSubmit()"
                                                   class="  text-xs  ps-3 border border-gray-200 bg-gray-50  rounded-lg">
                                        </td>
                                        <td class="px-6 py-4">

                                            @if($item['start_date'])
                                                <a href="{{ route('checkout.clean-item-date',[$item['id']]) }}" class="text-xs float-right">Limpar</a>
                                            @endif
                                            <div class="uppercase text-xs mb-1">Dt. Instalação</div>
                                            <input type="date"
                                                   name="date[{{ $item['id'] }}]"
                                                   value="{{ $item['start_date'] }}"
                                                   x-on:input="debounceSubmit()"
                                                   class="  text-xs w-full ps-3 border border-gray-200 bg-gray-50  rounded-lg">

                                        </td>
                                    </tr>

                                @endforeach

                                </tbody>
                                <tfoot>
                                <tr class="font-semibold text-gray-900 dark:text-white">
                                    <th scope="row" class="px-6 py-3 text-base" colspan="2">Total</th>
                                    <td class="px-6 py-3 font-normal" > {{ collect( $items )->sum('quantity') }} itens</td>
                                    <td class="px-6 py-3">
                                    </td>
                                    <td class="px-6 py-3">R$ {{ $cartService->getItemsSubtotal() }}</td>
                                </tr>
                                </tfoot>
                            </table>

                        </form>
                    </div>


                </div>

            </div>

        </div>

    </main>

</x-client-app-layout>
