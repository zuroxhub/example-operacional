<x-client-app-layout>

    @inject('cartService', 'App\Services\CartService')
    @php($checkoutAvaliable=true)

    @foreach($items as $item)
        @if(!$item['days'] or !$item['start_date'])
            @php($checkoutAvaliable=false)
        @endif
    @endforeach

    <main class=" w-full px-[var(--margin-x)] pb-8">

        <x-layout-client-header menu="checkout"/>

        <div class="p-3"> </div>

        <div class="container">
            <div class="text-4xl pl-3 pb-3 font-light opacity-50">Confirmação</div>
        </div>

        <div class="border-b mb-4 mt-5"></div>

        <div class="container">

            <div class="">

                <div class="min-h-[70vh]">
                    <div class="relative overflow-x-auto">
                        <form method="post" action="{{ route('checkout.update') }}"
                              x-data="{
                                      debounceSubmit: window.debounce(function() { $refs.submitButton.click() }, 600)
                                  }">
                            @csrf
                            <button type="submit" x-ref="submitButton" class="hidden">Submit</button>

                            <table class="w-full text-sm text-left text-gray-500 dark:text-gray-400">

                                <tbody>
                                @foreach($items as $item)
                                    <tr class="bg-white dark:bg-gray-800 border-b">
                                        <td class="px-1 py-4 w-[50px]">
                                            <img src="{{ asset( 'images/' . $item['item']['thumbnail']) }}" class=" max-w-[100px] m-auto max-h-[80px] h-max rounded-lg">
                                        </td>
                                        <th scope="row" class="px-6 py-4 font-medium text-gray-900 whitespace-nowrap dark:text-white">

                                            <div class="text-sm mb-1 text-gray-800">
                                                {{ $item['item']['name'] }}
                                            </div>
                                            <dl class="grid grid-cols-4 gap-2 text-xs mb-1">
                                                <dt class=" font-normal">SKU</dt>
                                                <dd class="col-span-3 ">{{ $item['item']['product_sku']}}</dd>
                                            </dl>
                                            <dl class="grid grid-cols-4 gap-2 text-xs mb-1">
                                                <dt class="font-normal">Vlr. Unit</dt>
                                                <dd class="col-span-3 ">R$ {{ \App\Services\General::currencyFormatter( $item['item']['daily_rental_price'] ) }}</dd>
                                            </dl>
                                        </th>
                                        <td class="px-6 py-4 font-bold text-center">
                                            {{ $item->quantity }} un.
                                        </td>
                                        <td class="px-6 py-4 font-bold text-center">
                                            {{ $item->days }} dia(s)
                                        </td>
                                        <td class="px-6 py-4">
                                            {{ \Carbon\Carbon::parse($item['start_date'])->format('d/m/Y') }}
                                        </td>
                                        <td class="px-6 py-4">
                                            {{ \Carbon\Carbon::parse($item['end_date'])->format('d/m/Y') }}
                                        </td>

                                        <td class="px-6 py-4">
                                            R$ {{ \App\Services\General::currencyFormatter( $item->quantity * $item['item']['daily_rental_price'] * $item['days']) }}
                                        </td>
                                    </tr>

                                @endforeach

                                </tbody>
                                <tfoot>
                                <tr class="font-semibold text-gray-900 dark:text-white">
                                    <th scope="row" class="px-6 py-3 text-base" colspan="2">Total</th>
                                    <td class="px-6 py-3 text-center" > {{ collect( $items )->sum('quantity') }} itens</td>
                                    <td class="px-6 py-3" colspan="3">
                                    </td>
                                    <td class="px-6 py-3">R$ {{ $cartService->getItemsSubtotal() }}</td>
                                </tr>
                                </tfoot>
                            </table>

                        </form>
                    </div>


                </div>

                <div class="grid grid-cols-2 gap-5 border-t pt-4">
                    <div>
                        <a  href="{{ route('checkout.index') }}" class="text-gray-900 bg-white hover:bg-gray-100 border border-gray-200 focus:ring-4 focus:outline-none focus:ring-gray-100  rounded-lg text-sm px-5 py-2.5 text-center inline-flex items-center dark:focus:ring-gray-600 dark:bg-gray-800 dark:border-gray-700 dark:text-white dark:hover:bg-gray-700 mr-2 mb-2">
                            Voltar ao checkout
                        </a>

                    </div>
                    <div class="text-center">
                        <a  href="{{ route('checkout.accepted') }}"  class="text-white w-full block bg-green-700 w-full hover:bg-green-800 focus:ring-4 focus:ring-green-300  rounded-lg text-sm px-5 py-2.5 mr-2 mb-2 dark:bg-green-600 dark:hover:bg-green-700 focus:outline-none dark:focus:ring-green-800">
                            Finalizar Pedido
                        </a>
                    </div>
                </div>


            </div>


        </div>

    </main>

</x-client-app-layout>
