<x-client-app-layout>

    <!-- Main Content Wrapper -->
    <main class=" w-full px-[var(--margin-x)] pb-8">

        <x-layout-client-header menu="business"/>
        <div class="pt-5">
            <div class="card col-span-12 lg:col-span-8 xl:col-span-9">

                <div class="mt-3 flex items-center  justify-between px-4 sm:px-5">
                    <h2 class="text-base font-medium tracking-wide text-slate-700 line-clamp-1 dark:text-navy-100">
                        Informações da conta
                    </h2>
                </div>

                <div class="p-3 ">

                    <dl class="p-3">
                        <dt class="uppercase">Nome da Empresa</dt>
                        <dd class="fw-bold">{{ $businessClient['name'] }}</dd>
                    </dl>

                    <dl class="p-3">
                        <dt>E-mai</dt>
                        <dd class="fw-bold">{{ $businessClient['email'] }}</dd>
                    </dl>
                    <dl class="p-3">
                        <dt>Telefone</dt>
                        <dd class="fw-bold">{{ $businessClient['phone'] }}</dd>
                    </dl>

                    <dl class="p-3">
                        <dt>Endereço</dt>
                        <dd class="fw-bold">{{ $businessClient['address'] }}</dd>
                    </dl>

                    <dl class="p-3">
                        <dt>Data de Cadastro</dt>
                        <dd class="fw-bold">{{ $businessClient['created_at'] }}</dd>
                    </dl>


                    <div class="p-3">
                        <a href="#">Modificar</a>
                    </div>

                </div>
            </div>
        </div>

    </main>

</x-client-app-layout>
