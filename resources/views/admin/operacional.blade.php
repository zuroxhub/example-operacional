<x-admin-layout>

    <!-- Main Content Wrapper -->
    <main class=" w-full px-[var(--margin-x)] pb-8">

        <x-layout-admin-header menu="dashboard"/>

        <div class="p-3"> </div>

        <div class="container">

            <div class="grid grid-cols-2">
                <div>
                    <div class="text-4xl pl-3 pb-3 font-light opacity-50">Operacional</div>
                </div>
                <div>
                    <form action="{{ route('admin.operacional') }}" method="get">
                        <input type="text" name="rentalId" class="border border-gray-200 rounded text-sm px-3 w-full" value="{{ request()->get('rentalId')  }}" placeholder="Abrir pedido">
                    </form>
                </div>
            </div>
        </div>


        <div class="text-sm  font-medium text-center text-gray-500 border-b border-gray-200 dark:text-gray-400 dark:border-gray-700">
            <ul class="flex flex-wrap -mb-px container">
                <li class="mr-2">
                    <a href="{{ route('admin.operacional') }}?viewMode=all" class=" @if( !request()->has('viewMode') or request()->get('viewMode')=='all' ) tab-active @else tab-default @endif">

                        @if(!request('rentalId'))
                            Todos
                        @else
                            Busca Simples
                        @endif

                    </a>
                </li>
                <li class="mr-2">
                    <a href="{{ route('admin.operacional') }}?viewMode=processing" class="  @if( request()->get('viewMode')=='processing' ) tab-active @else tab-default @endif">Processamento</a>
                </li>
                <li class="mr-2">
                    <a href="{{ route('admin.operacional') }}?viewMode=active" class="  @if( request()->get('viewMode')=='active' ) tab-active @else tab-default @endif">Ativos</a>
                </li>
                <li class="mr-2">
                    <a href="{{ route('admin.operacional') }}?viewMode=closed" class="  @if( request()->get('viewMode')=='closed' ) tab-active @else tab-default @endif">Finalizados</a>
                </li>
            </ul>
        </div>

        @if($avaliableStatus)
        <div class="text-xs mb-5 font-normal text-center text-gray-500 border-b border-gray-200 dark:text-gray-400 dark:border-gray-700">
            <ul class="flex flex-wrap -mb-px container">
                @foreach($avaliableStatus as $aStatus)
                <li class="mr-2">
                    <a href="{{ route('admin.operacional') }}?viewMode={{ request()->get('viewMode') }}&status={{ $aStatus }}" class="  @if( request()->get('status')==$aStatus ) tab-active @else tab-default @endif">

                        {{ \App\Models\Rental::getStatusLabels()[$aStatus] }}

                    </a>
                </li>
                @endforeach
            </ul>
        </div>

        <div class="container text-xs">

            <div class="relative overflow-x-auto">
                <table class="w-full text-sm text-left text-gray-500 dark:text-gray-400">
                    <thead class="text-xs text-gray-700 uppercase bg-gray-50 dark:bg-gray-700 dark:text-gray-400">
                    <tr>
                        <th scope="col" class="px-6 py-3">
                            #
                        </th>
                        <th scope="col" class="px-6 py-3">
                            Parceiro
                        </th>
                        <th scope="col" class="px-6 py-3">

                        </th>
                        <th scope="col" class="px-6 py-3 min-w-[400px]">
                            Descrição
                        </th>
                        <th scope="col" class="px-6 py-3">
                            Status
                        </th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($rentals as $rental)
                    <tr class="bg-white border-b dark:bg-gray-800 dark:border-gray-700 hover:bg-gray-100 transition-all cursor-pointer"
                        onclick="window.loadRental ({{ $rental['id'] }})"
                        data-drawer-placement="right"
                        data-drawer-target="drawer-rental" data-drawer-show="drawer-rental" aria-controls="drawer-rental"
                    >
                        <td class="px-6 py-4">
                            <div class="font-bold text-base text-gray-800">#{{ $rental['id'] }}</div>
                            <div class="text-xs">{{  $rental['created_at']->format('d/m H:i')  }}</div>
                            <div class="text-xs">{{  $rental['created_at']->diffForHumans()  }}</div>
                        </td>
                        <th scope="row" class="px-6 py-4 font-normal text-xs text-gray-900 whitespace-nowrap dark:text-white">
                            <div class="text-sm  font-medium">{{ $rental['client']['name'] }}</div>
                            <div>{{ $rental['client']['phone'] }}</div>
                            <div>{{ $rental['client']['address'] }}</div>
                        </th>
                        <td class="px-6 py-4 ">
                            <img src="{{ asset('images/'.$rental['item']['thumbnail']) }}" class="max-w-[60px] m-auto">


                        </td>

                        <td class="px-6 py-4">
                            <div>{{ $rental['item']['category']['name'] }}</div>
                            <div class="mb-2 font-semibold text-gray-800 uppercase">{{ $rental['item']['name'] }}</div>
                            <div>
                                <dl>
                                    <dd class="bg-green-100 text-green-800 text-xs font-medium mr-2 px-2.5 py-0.5 rounded dark:bg-green-900 dark:text-green-300">{{ $rental['unit']['serial_number'] }}</dd>
                                </dl>
                            </div>
                        </td>
                        <td class="px-6 py-4">
                            <div class=" whitespace-nowrap  rounded-lg p-3 {{ \App\Models\Rental::getStatusStyle()[$rental['status']]}}">
                                {{ \App\Models\Rental::getStatusLabels()[$rental['status']]}}
                            </div>
                        </td>
                    </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>


            <div class="mt-4">
                {{ $rentals->links() }}
            </div>

            @else
                <div class="container ">
                <div class="p-5 text-center text-gray-400">
                    Não há itens aqui
                </div>
                </div>
            @endif
{{--            @foreach($rentals as $rental)
            <pre>{{ print_r($rental->toArray()) }}</pre>
            @endforeach--}}
        </div>



        <!-- drawer component -->
        <div id="drawer-rental" class="fixed top-0 right-0 z-40 h-screen p-4 overflow-y-auto transition-transform translate-x-full bg-white w-80 dark:bg-gray-800 w-[600px]" tabindex="-1" aria-labelledby="drawer-right-label">
            <h5 id="drawer-right-label" class="inline-flex items-center mb-4 text-base font-semibold text-gray-500 dark:text-gray-400"><svg class="w-4 h-4 mr-2.5" aria-hidden="true" xmlns="http://www.w3.org/2000/svg" fill="currentColor" viewBox="0 0 20 20">
                    <path d="M10 .5a9.5 9.5 0 1 0 9.5 9.5A9.51 9.51 0 0 0 10 .5ZM9.5 4a1.5 1.5 0 1 1 0 3 1.5 1.5 0 0 1 0-3ZM12 15H8a1 1 0 0 1 0-2h1v-3H8a1 1 0 0 1 0-2h2a1 1 0 0 1 1 1v4h1a1 1 0 0 1 0 2Z"/>
                </svg>Operational</h5>
            <button type="button" data-drawer-hide="drawer-rental" aria-controls="drawer-rental" class="text-gray-400 bg-transparent hover:bg-gray-200 hover:text-gray-900 rounded-lg text-sm w-8 h-8 absolute top-2.5 right-2.5 inline-flex items-center justify-center dark:hover:bg-gray-600 dark:hover:text-white" >
                <svg class="w-3 h-3" aria-hidden="true" xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 14 14">
                    <path stroke="currentColor" stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="m1 1 6 6m0 0 6 6M7 7l6-6M7 7l-6 6"/>
                </svg>
                <span class="sr-only">Close menu</span>
            </button>

            <div id="conteudo"></div>

        </div>

    </main>

</x-admin-layout>
