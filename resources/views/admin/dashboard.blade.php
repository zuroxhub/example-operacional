<x-admin-layout>

    <!-- Main Content Wrapper -->
    <main class=" w-full px-[var(--margin-x)] pb-8">

        <x-layout-admin-header menu="dashboard"/>

        <div class="p-3"> </div>

        <div class="container">
            <div class="text-4xl pl-3 pb-3 font-light opacity-50">Dashboard</div>
        </div>
        <div class="border-b mb-5"></div>

        <div class="container">
            <div class="grid gap-2 my-4 mx-3 grid-cols-2">

                <div class="col-span-2">

                    <div class="grid grid-cols-3 gap-3 ">

                        Olá, {{ Auth::user()->name }}!

                    </div>

                </div>
            </div>
        </div>

        <div class="container">
            <div class="text-4xl pl-3 pt-5 pb-3 font-light opacity-50"> Sessão</div>
        </div>

        <div class="border-b mb-5"></div>

        <div class="container">

            <a href="{{ route('admin.logout') }}"
               class="p-3 text-base"
               onclick="event.preventDefault();document.getElementById('formlogout').submit();">Sair</a>
            <form method="POST" id="formlogout" action="{{ route('admin.logout') }}">
                @csrf
            </form>

        </div>


    </main>

</x-admin-layout>
