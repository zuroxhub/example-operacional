
<div class=" text-xs mb-4">

    <div class="grid grid-cols-2">
        <dl class="mb-3">
            <dt>Locação</dt>
            <dd class="font-semibold">{{ $rental['id'] }}</dd>
        </dl>
        <dl class="mb-3">
            <dt>Iniciado</dt>
            <dd class="font-semibold">{{ \Carbon\Carbon::parse($rental['created_at'])->format('d/m/y H:i') }}</dd>
        </dl>
        <dl>
            <dt>Status</dt>
            <dd class="font-semibold">{{ \App\Models\Rental::getStatusLabels()[$rental['status']] }}</dd>
        </dl>
    </div>

</div>

<div class="card text-xs mb-4">

    <div class="font-semibold text-xl mb-2">Parceiro</div>
    <dl class="mb-3">
        <dt>Nome do Parceiro</dt>
        <dd class="font-semibold">{{ $rental['client']['name'] }}</dd>
    </dl>
    <dl class="mb-3">
        <dt>Telefone</dt>
        <dd class="font-semibold">{{ $rental['client']['phone'] }}</dd>
    </dl>
    <dl>
        <dt>Endereço</dt>
        <dd class="font-semibold">{{ $rental['client']['address'] }}</dd>
    </dl>
</div>


<div class="card text-xs mb-4">
    <div class="font-semibold text-xl mb-2">Equipamento</div>
    <dl class="mb-3">
        <dt>Descrição</dt>
        <dd class="font-semibold">{{ $rental['item']['name'] }}</dd>
    </dl>
    <dl class="mb-3">
        <dt>SKU</dt>
        <dd class="font-semibold">{{ $rental['item']['product_sku'] }}</dd>
    </dl>
    <dl class="mb-3">
        <dt>Serial</dt>
        <dd class="font-semibold">{{ $rental['unit']['serial_number'] }}</dd>
    </dl>
</div>

@if($status)

    <form id="formRentalUpdate">
        @csrf
        <input type="hidden" name="rentalId" value="{{ $rental['id'] }}">
        <input type="hidden" name="operatorId" value="{{ \Illuminate\Support\Facades\Auth::user()->id }}">

        <div class=" block  p-3 mb-4 border border-gray-200 rounded-lg shadow-sm hover:shadow hover:bg-gray-50 dark:bg-gray-800 dark:border-gray-700 transition-all mt-4 rounded-lg bg-gray-200 p-4">
            <div class="font-semibold text-xl mb-2">Atualização</div>
            <div class="text-sm mb-2">Selecione o status e descreva a operação.</div>
            <select name="rentalStatus" class="border border-gray-200 w-full rounded-lg text-sm">
                <option disabled>Status da locação</option>
                @foreach($status as $status_code)
                    <option value="{{ $status_code }}" @if($rental['status']==$status_code) selected @endif>{{  \App\Models\Rental::getStatusLabels()[$status_code] }}</option>
                @endforeach
            </select>
            <textarea rows="3" name="operationDescription" class="border border-gray-200 w-full rounded-lg text-sm mt-3" placeholder="Descrição da operação"></textarea>
            <button type="button" onclick="submitRentalUpdate({{ $rental['id'] }})" class="text-white mt-4 bg-blue-700 hover:bg-blue-800 focus:ring-4 focus:ring-blue-300 font-medium rounded-lg text-xs px-5 py-1.5 mr-2 mb-2 dark:bg-blue-600 dark:hover:bg-blue-700 focus:outline-none dark:focus:ring-blue-800">
                Executar
            </button>
        </div>
    </form>

@endif


    <div class="font-semibold text-xl mb-2">Histórico</div>

    <div class="border rounded-lg p-3 bg-gray-100 ovexrflow-y-scroll">
        @php($rentalLog=collect($rental['log'])->sortByDesc('id'))
        @foreach($rentalLog as $log)

            <div class="border rounded-lg bg-white mb-3">

                <div class="text-sm px-3 pt-3 text-gray-400">{{ \Carbon\Carbon::parse($log['created_at'])->format('d/m/Y H:i:s') }} - {{ \Carbon\Carbon::parse($log['created_at'])->diffForHumans() }}</div>

                <div class="grid grid-cols-2 gap-3 ">
                    <dl class="m-3">
                        <dt class="uppercase text-gray-500 text-xs">Status</dt>
                        <dd class="font-semibold   {{ \App\Models\Rental::getStatusStyle() [$log['status']]  }} block">
                            {{ \App\Models\Rental::getStatusLabels() [$log['status']] }}

                        </dd>
                    </dl>
                    <dl class="m-3">
                        <dt class="uppercase text-gray-500 text-xs">Operador</dt>
                        <dd class="font-semibold text-gray-800">
                            {{ $log['operator']['name'] }}
                        </dd>
                    </dl>
                </div>
                <div class="border-t p-3 rounded">
                    <span class="uppercase text-gray-500 text-xs">Descrição da operação</span>
                    <div>
                        {!! nl2br($log['description'])  !!}
                    </div>

                </div>
            </div>

            {{--
                <div class="bg-gray-50 border-gray-300 text-xs p-3 rounded-lg mb-3">


                    <div class="mb-3 text-sm p-2 font-semibold">
                        {{ nl2br($log['description'] )}}
                    </div>
                    <div>
                        <img class="inline-block w-[16px] mr-1 opacity-50" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAB4AAAAeCAYAAAA7MK6iAAAAAXNSR0IArs4c6QAAAlZJREFUSEulV4uxAyEIXLpILSnjarkyrpYr42pJF75BUUFBk3nO5HcqCOwuhtAGgZCQCJA3NTM8ymv6TrtQfsma1VJrYVppH0SGspHNZA6snssJ0AmlPtKWl15s2EFyfoj4G2fap873nLlNir6p5Q9Fl5rwjv+NDYh84wzk7WjRvAGcsvwC8OwBFRunL1BaHfKnGvQQ0rFmlaboDKUdAjnKM6AKR86vNZPWqA5j/yyqwek+2vwP9VY1jqBPH038oa4PCIerYhsAFMeTLkhtSiifBVmelB3HwtJnBh77Rs3TUmN/HCA8o7YbCLqyXum0FyRG9M2pkbRzbS8CHoPqvZ2W3ZDHm16xZL/W/8iOqfHUMAhvJHC0mTJforfynrNSX/PeBXDuhOyUR+OrAiPPFfUqQ7DQLDLNbvnF3/XaUDJvdKfV8Es5qVEJh+nj3Az0+owPIDEY863C4/GJ5KK4R024pQTVuIf8PCcRDwcFilb3fAt6Q+wcsrQi/CWoPgk4B93uERes8OGayhlUy2afsw7yCGnjuNEv2yTgslefHqBT25ic2RDh4rQTwGDsySW8lJRyhi4gNYCNPLYNIZLSLutaMsdmUlM91Tcfb9DSG6UeFXzTVdep/ktWW4D1iLmlss3exbICquxoPsayP7nWHNdRc8QVrJpaxWXUPbgrGRdO2hV3Fa3olOcHkL9z62QcTOZc7igQzxeBfojcLKwq5cm6xyqe8hQ2CacEwyPTswPidxT2vxClT64dO951aaabyeTe1qf/NzM1ll0ObfdAs5ti5teTEf4AHRH8JZ4Ed3YAAAAASUVORK5CYII=">
                        {{ $log['operator']['name'] }}
                        -
                        {{ \Carbon\Carbon::parse($log['created_at'])->format('d/m/Y H:i:s') }}
                    </div>

                </div>--}}

        @endforeach
    </div>


