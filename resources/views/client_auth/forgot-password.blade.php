<x-client-guest-layout>

    <div>
        <main class="grid w-full grow grid-cols-1 place-items-center">
            <div class="w-full max-w-[35rem] p-4 sm:px-5">
                <div class="text-center">
                    <div class="my-4">
                        <h2 class="text-2xl font-semibold text-primary ">
                            Recuperação de Senha
                        </h2>
                        <p class="text-slate-400 dark:text-navy-300">
                            {{ __('Forgot your password? No problem. Just let us know your email address and we will email you a password reset link that will allow you to choose a new one.') }}
                        </p>
                    </div>
                </div>

                <div class="card border bg-gray-50 mt-5 rounded-lg p-5 lg:p-7">


                    <!-- Session Status -->
                    <x-auth-session-status class="mb-4" :status="session('status')" />

                    <form method="POST" action="{{ route('client.password.email') }}">
                    @csrf

                    <!-- Email Address -->
                        <div>
                            <x-input-label for="email" class="mb-2 uppercase text-xs font-normal text-gray-400"  :value="__('Email')" />
                            <x-text-input id="email" class="block mt-1 w-full" type="email" name="email" :value="old('email')" required autofocus />
                            <x-input-error :messages="$errors->get('email')" class="mt-2" />
                        </div>

                        <div class="flex items-center justify-end mt-4">
                            <x-primary-button>
                                {{ __('Email Password Reset Link') }}
                            </x-primary-button>
                        </div>
                    </form>

                </div>
            </div>
        </main>
    </div>
</x-client-guest-layout>
