<x-client-guest-layout>



    <div

    >
        <main class="grid w-full grow grid-cols-1 place-items-center">
            <div class="w-full max-w-[26rem] p-4 sm:px-5">
                <div class="text-center">

                    <div class="mt-4">
                        <h2
                            class="text-2xl font-semibold text-slate-600 dark:text-navy-100"
                        >
                            Cadastro
                        </h2>
                        <p class="text-slate-400 dark:text-navy-300">
                            Faça o seu cadastro rápido e seguro!
                        </p>
                    </div>
                </div>


                <div class="card border bg-gray-50 mt-5 rounded-lg p-5 lg:p-7">

                    <!-- Session Status -->
                    <x-auth-session-status class="mb-4" :status="session('status')" />

                    <form method="POST" action="{{ route('client.register') }}">
                    @csrf

                    <!-- Name -->
                        <div>
                            <x-input-label for="name" class="mb-2 uppercase text-xs font-normal text-gray-400" :value="__('Name')" />
                            <x-text-input id="name"  class="form-input peer w-full rounded-lg border border-slate-300 bg-transparent px-3 py-2  placeholder:text-slate-400/70 hover:z-10 hover:border-slate-400 focus:z-10 focus:border-primary dark:border-navy-450 dark:hover:border-navy-400 dark:focus:border-accent"
                                      type="text" name="name" :value="old('name')" required autofocus autocomplete="name" />
                            <x-input-error :messages="$errors->get('name')" class="mt-2" />
                        </div>

                        <!-- Email Address -->
                        <div class="mt-4">
                            <x-input-label for="email" class="mb-2 uppercase text-xs font-normal text-gray-400"  :value="__('Email')" />
                            <x-text-input id="email"  class="form-input peer w-full rounded-lg border border-slate-300 bg-transparent px-3 py-2  placeholder:text-slate-400/70 hover:z-10 hover:border-slate-400 focus:z-10 focus:border-primary dark:border-navy-450 dark:hover:border-navy-400 dark:focus:border-accent"
                                      type="email" name="email" :value="old('email')" required autocomplete="username" />
                            <x-input-error :messages="$errors->get('email')" class="mt-2" />
                        </div>

                        <!-- Password -->
                        <div class="mt-4">
                            <x-input-label for="password" class="mb-2 uppercase text-xs font-normal text-gray-400"  :value="__('Password')" />

                            <x-text-input id="password"  class="form-input peer w-full rounded-lg border border-slate-300 bg-transparent px-3 py-2  placeholder:text-slate-400/70 hover:z-10 hover:border-slate-400 focus:z-10 focus:border-primary dark:border-navy-450 dark:hover:border-navy-400 dark:focus:border-accent"

                                          type="password"
                                          name="password"
                                          required autocomplete="new-password" />

                            <x-input-error :messages="$errors->get('password')" class="mt-2" />
                        </div>

                        <!-- Confirm Password -->
                        <div class="mt-4">
                            <x-input-label for="password_confirmation" class="mb-2 uppercase text-xs font-normal text-gray-400"  :value="__('Confirm Password')" />

                            <x-text-input id="password_confirmation"  class="form-input peer w-full rounded-lg border border-slate-300 bg-transparent px-3 py-2  placeholder:text-slate-400/70 hover:z-10 hover:border-slate-400 focus:z-10 focus:border-primary dark:border-navy-450 dark:hover:border-navy-400 dark:focus:border-accent"

                                          type="password"
                                          name="password_confirmation" required autocomplete="new-password" />

                            <x-input-error :messages="$errors->get('password_confirmation')" class="mt-2" />
                        </div>

                        <div class="">


                            <x-primary-button     class="btn mt-5 w-full bg-primary font-medium text-white hover:bg-primary-focus focus:bg-primary-focus active:bg-primary-focus/90 dark:bg-accent dark:hover:bg-accent-focus dark:focus:bg-accent-focus dark:active:bg-accent/90"
                            >
                                {{ __('Register') }}
                            </x-primary-button>
                        </div>
                    </form>


                </div>
            </div>
        </main>
    </div>


</x-client-guest-layout>
