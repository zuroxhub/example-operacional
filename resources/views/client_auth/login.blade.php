<x-client-guest-layout>

    <div>
        <main class="grid w-full grow grid-cols-1 place-items-center">
            <div class="w-full max-w-[26rem] p-4 sm:px-5">
                <div class="text-center">
                    <div class="my-4">
                        <h2 class="text-2xl font-semibold text-primary ">
                            Autenticação
                        </h2>
                        <p class="text-slate-400 dark:text-navy-300">
                            Informe o seu usuário e senha para entrar.
                        </p>
                    </div>
                </div>

                <div class="card border bg-gray-50 mt-5 rounded-lg p-5 lg:p-7">

                <x-auth-session-status class="mb-4" :status="session('status')" />

                <form method="POST" action="{{ route('client.login') }}">
                @csrf

                    <div>
                        <x-input-label for="email" class="mb-2 uppercase text-xs font-normal text-gray-400"  :value="__('Email')" />
                        <x-text-input id="email"
                                      class="form-input peer w-full rounded-lg border border-slate-300 bg-transparent px-3 py-2  placeholder:text-slate-400/70 hover:z-10 hover:border-slate-400 focus:z-10 focus:border-primary dark:border-navy-450 dark:hover:border-navy-400 dark:focus:border-accent"
                                      type="email" name="email" :value="old('email')" required autofocus autocomplete="username" />
                        <x-input-error :messages="$errors->get('email')" class="mt-2" />
                    </div>

                    <div class="mt-4">
                        <x-input-label for="password" class="mb-2 uppercase text-xs font-normal text-gray-400"  :value="__('Password')" />

                        <x-text-input id="password"
                                      class="form-input peer w-full rounded-lg border border-slate-300 bg-transparent px-3 py-2 placeholder:text-slate-400/70 hover:z-10 hover:border-slate-400 focus:z-10 focus:border-primary dark:border-navy-450 dark:hover:border-navy-400 dark:focus:border-accent"

                                      type="password"
                                      name="password"
                                      required autocomplete="current-password" />

                        <x-input-error :messages="$errors->get('password')" class="mt-2" />
                    </div>

                    <div class="mt-2 ">
                        @if (Route::has('password.request'))
                            <a class=" text-xs my-3 d-block mt-3 text-gray-600 hover:text-gray-900 rounded-md focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500" href="{{ route('client.password.request') }}">
                                {{ __('Esqueceu a senha?') }}
                            </a>
                        @endif

                        <x-primary-button
                            class="btn mt-5 w-full bg-primary font-medium text-white hover:bg-primary-focus focus:bg-primary-focus active:bg-primary-focus/90 dark:bg-accent dark:hover:bg-accent-focus dark:focus:bg-accent-focus dark:active:bg-accent/90"

                        >
                            {{ __('Log in') }}
                        </x-primary-button>
                    </div>
                </form>




            </div>
        </main>
    </div>


</x-client-guest-layout>
