import './bootstrap';
import './marketplace';
import 'flowbite';

window.loadRental = function ( itemID ) {
    fetch('/admin/rental/ajax/details/' + itemID, {
        method: 'GET',
        headers: {
            'X-CSRF-TOKEN': document.querySelector('meta[name="csrf-token"]').content
        },
    })
        .then(response => response.text())

        .then(data => {
            document.getElementById('conteudo').innerHTML = data
        })
        .catch(error => {
            console.error('Erro ao atualizar o item do carrinho:', error);
        })
        .finally(() => {
            this.processing = false;
        });
}

window.submitRentalUpdate = function( rentalID ) {
    const formElement = document.getElementById('formRentalUpdate');
    const formData = new FormData(formElement);

    fetch('/admin/rental/ajax/update/' + rentalID, {
        method: 'POST',
        headers: {
            'X-CSRF-TOKEN': document.querySelector('meta[name="csrf-token"]').content
        },
        body: formData
    })
        .then(data => data.json())
        .then(data => {

            console.log(data);

            if( data.success === true ){

                Swal.fire({
                    title: 'Sucesso',
                    text: data.message,
                    icon: 'success',
                    timer: 1000
                })

                setTimeout( () => {
                    window.location = window.location.href;
                },1500)

            }else{

                Swal.fire({
                    title: 'Ops',
                    text: data.message,
                    icon: 'error',
                    timer: 1000
                })
            }

            window.loadRental( rentalID );
        })
        .catch(error => {
            console.error('Erro ao enviar o formulário:', error);
        });
}

window.debounce = function (func, wait) {
    let timeout;
    return function (...args) {
        clearTimeout(timeout);
        timeout = setTimeout(() => {
            func.apply(this, args);
        }, wait);
    };
}

import Alpine from 'alpinejs';

window.Alpine = Alpine;

Alpine.start();
