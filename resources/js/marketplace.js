window.marketplaceItem = function ( itemID ) {
    axios.get('/client/marketplace/item/' + itemID)
        .then(response => {
            const data = response.data;
            document.getElementById('conteudo').innerHTML = data;
        })
        .catch(error => {
            console.error('Erro ao carregar a rota:', error);
        });
}
