import './bootstrap';
import './marketplace';
import 'flowbite';



window.calculator = function ( price, itemId ) {
    return {
        quantity: 1,
        itemId: itemId,
        processing: false,
        get total() {
            return (this.quantity * price).toFixed(2);
        },
        get quantityDisplay() {
            return new Intl.NumberFormat('pt-BR').format( (this.quantity * price).toFixed(2) );
        },
        increment() {
            this.quantity++;
            document.getElementById('quantityInput').value = this.quantity;
        },
        decrement() {
            if (this.quantity > 1) {
                this.quantity--;
            }
            document.getElementById('quantityInput').value = this.quantity;
        }
    }
}

/**
 * Add to Cart
 * Adiciona um item ao carrinho partindo de um formulário simples
 *
 * @param event
 * @param itemId
 */
window.addToCart = function ( event, itemId ) {

    event.preventDefault()

    const form = document.getElementById('addItemForm' + itemId);
    const formData = new FormData(form);

    fetch('/client/cart/ajax/add', {
        method: 'POST',
        headers: {
            'X-CSRF-TOKEN': document.querySelector('meta[name="csrf-token"]').content
        },
        body: formData
    })
        .then(response => response.json())
        .then(data => {

            window.refreshTotals();

            Swal.fire({
                title: 'Success',
                text: 'Item foi registrado!',
                icon: 'success',
                confirmButtonText: 'Continuar'
            })

        })
        .catch(error => {
            console.error('Erro ao enviar o formulário:', error);
        });
}

window.refreshTotals = function ( event, itemId ) {

    fetch('/client/cart/ajax/total', {
        method: 'GET',
        headers: {
            'Content-Type': 'application/json',
            'X-CSRF-TOKEN': document.querySelector('meta[name="csrf-token"]').content
        },
    })
        .then(response => response.json())
        .then(response => {
            document.getElementById('totalCart').innerText = response.total;
        })
        .catch(error => {
            console.error('Erro ao atualizar o toptal de item ao carrinho:', error);
        });
}

window.cartItemManager = function(itemId, initialQuantity) {
    return {
        itemId: itemId,
        quantity: initialQuantity,
        processing: false,

        updateQuantity: window.debounce(function() {
            if (this.processing) return;
            this.processing = true;

            fetch('/client/cart/ajax/update', {
                method: 'PUT',
                headers: {
                    'Content-Type': 'application/json',
                    'X-CSRF-TOKEN': document.querySelector('meta[name="csrf-token"]').content
                },
                body: JSON.stringify({
                    item_id: this.itemId,
                    quantity: this.quantity
                })
            })
                .then(response => response.json())
                .then(data => {

                    this.quantity = data.message.original.quantityUpdated;
                    window.refreshTotals();

                })
                .catch(error => {
                    console.error('Erro ao atualizar o item do carrinho:', error);
                })
                .finally(() => {
                    this.processing = false;
                });
        }, 500),  // 300ms delay


        increment() {
            this.quantity++;
            this.updateQuantity();
        },

        decrement() {
            if (this.quantity > 1) {
                this.quantity--;
                this.updateQuantity();
            }
        }
    }
}

window.debounce = function (func, wait) {
    let timeout;
    return function (...args) {
        clearTimeout(timeout);
        timeout = setTimeout(() => {
            func.apply(this, args);
        }, wait);
    };
}

import Alpine from 'alpinejs';

window.Alpine = Alpine;


Alpine.start();
