# Operacional
O projeto tem como objetivo fornecer uma plataforma onde os clientes da empresa podem solicitar a instalação de novos equipamentos e acompanhar em tempo real o processo de instalação.

## ** NOTA **
> A demonstração serve apenas para refletir sobre conceitos, pois não há um bom mapeamento dos status e detalhamento dos processos.
>
> Devido à falta de tempo, eu gostaria de ter desenvolvido mais padronizado. mas decidir fazer um "MIX".
> 
> Alguns elementos das páginas podem não estar bem alinhados, mas espero que compreendam.
> 
> **Mas eu encaro este projeto como um hackathon. 👨‍💻**

## Preparação do Ambiente

Preparação do ambiente para execução em servidor PHP local com Sqlite.
> Obs: o .env está incluso para agilizar o processo.

```
git clone https://gitlab.com/zuroxhub/example-operacional
cd example-operacional
composer i
touch database/database.sqlite
php artisan migrate
php artisan db:seed
npm i
npm run build

php artisan serve
 -- ou --
php artisan serve --host x.x.x.x
```

## Apresentação

- Foi criado um segundo ambiente de autenticação para os clientes do projeto, isolando assim o sistema de usuários do restante do sistema.
- O método de "desenvolvimento ágil" neste projeto se traduziu na implementação das Factories para a criação rápida dos modelos do banco de dados.
- No frontend, usei o Alpinejs no componente de estimativa de dias/locação e no carrinho. também foi usado Javascript para alguns métodos. Devido ao mix de tecnologias, algumas páginas apresentam abordagens diferentes para resolver os problemas.


## Administração do Projeto

A Autenticação da area administrativa só é possivel através do link 

```
/admin/login
```

Credencias do administrador
```
email: admin@admin.com
senha: password123
```


## Screenshots


#### Landing Page
Página onde o usuário pode efetuar o cadastro & login

**Ao efetuar o cadastro o usuário será associado a uma empresa aleatória do banco de dados **

![alt text](./doc/c1.png)

#### Cliente - Dashboard
- Resumo com os itens ativos em locação
- Informações básicas da empresa
- Botão Sair (Logout)

![alt text](./doc/c2.png)

#### Cliente - Locações
- Navegação pelos contratos relacionados à empresa

![alt text](./doc/c3.png)
#### Cliente - Locação
- Detalhes do equipamento 
- Histórico de ações na locação

![alt text](./doc/c4.png)

#### Cliente - Marketetplace
- Navegação no catálogo de produtos disponíveis para locação
- Calculador de diária ao adicionar o item no pedido

![alt text](./doc/c5.png)

#### Cliente - Carrinho
- Atualiza a quantidade de itens para locação, com uma validação sobre a quantidade disponível em estoque

![alt text](./doc/c6.png)

#### Cliente - Preparação do Pedido
- Configuração de dias e data para instalação do equipamento

![alt text](./doc/c7.png)
#### Cliente - Confirmação do pedido
- O cliente tem a oportunidade de revisar o pedido, incluindo a data de renovação

![alt text](./doc/c8.png)

#### Admin - Dashboard
![alt text](./doc/c9.png)

#### Admin - Operacional
- Navegação de locações

![alt text](./doc/c10.png)

#### Admin - Locação
- Detalhes da locação
- Atualização do Histórico

![alt text](./doc/c11.png)
