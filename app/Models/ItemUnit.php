<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ItemUnit extends Model
{
    use HasFactory;

    const STATUS_AVAILABLE = 'available';
    const STATUS_RESERVED = 'reserved';
    const STATUS_RENTED = 'rented';

    protected $fillable = [
        'item_id',
        'status',
        'serial_number',
        'rental_id'
    ];

    /**
     * Relação com o Item a que esta unidade pertence.
     */
    public function item()
    {
        return $this->belongsTo(Item::class);
    }

    /**
     * Relação com o Rental ao qual esta unidade está atualmente associada.
     */
    public function rental()
    {
        return $this->belongsTo(Rental::class);
    }

    /**
     * Determina se esta unidade está atualmente em locação.
     */
    public function isRented()
    {
        return !is_null($this->rental_id);
    }

    /**
     * Associa esta unidade a um Rental.
     *
     * @param Rental $rental
     */
    public function rentTo(Rental $rental)
    {
        $this->rental_id = $rental->id;
        $this->save();
    }

    /**
     * Desassocia esta unidade de qualquer Rental.
     */
    public function returnUnit()
    {
        $this->rental_id = null;
        $this->save();
    }
}
