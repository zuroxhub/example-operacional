<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class ClientUser extends Authenticatable
{
    use Notifiable;

    protected $fillable = ['name', 'email', 'password','client_id'];

    protected $hidden = ['password', 'remember_token'];

    public function getAuthPassword()
    {
        return $this->password;
    }

    public function businessClient()
    {
        return $this->hasOne(Client::class,'id','client_id');
    }
}


