<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class RentalLog extends Model
{
    use HasFactory;

    protected $fillable = [
        'rental_id',
        'operator_id',
        'status',
        'description',
        'created_at',
    ];

    public $timestamps = false;

    public function operator()
    {
        return $this->hasOne(User::class,'id','operator_id');
    }

}
