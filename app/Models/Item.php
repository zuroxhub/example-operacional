<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Item extends Model
{
    use HasFactory;

    protected $fillable = [
        'name',
        'category_id',
        'description',
        'thumbnail',
        'product_sku',
        'stock_quantity',
        'daily_rental_price'
    ];

    public $casts = [

    ];

    public function category()
    {
        return $this->belongsTo(Category::class);
    }

    public function itemUnits()
    {
        return $this->hasMany(ItemUnit::class);
    }

    public function availableUnits()
    {
        return $this->itemUnits()->where('status', ItemUnit::STATUS_AVAILABLE)->count();
    }

}
