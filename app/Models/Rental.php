<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Rental extends Model
{
    use HasFactory;

    const STATUS_REQUEST_NEW = 'request_new';
    const STATUS_REQUEST_ACCEPTED = 'request_accepted';
    const STATUS_REQUEST_REFUSED = 'request_refused';
    const STATUS_PREPARATION_ITEM_SEPARATION = 'preparation_item_separation';
    const STATUS_PREPARATION_CLIENT_IDENTIFICATION = 'preparation_client_identification';
    const STATUS_ALLOCATION_ROUTE_PREPARATION = 'allocation_route_preparation';
    const STATUS_ALLOCATION_ON_ROUTE = 'allocation_on_route';
    const STATUS_ALLOCATION_SUCCESSFUL_INSTALLATION = 'allocation_successful_installation';
    const STATUS_ALLOCATION_INSTALLATION_ISSUE = 'allocation_installation_issue';
    const STATUS_CONTRACT_ACTIVE = 'contract_active';
    const STATUS_CONTRACT_CANCELLED = 'contract_cancelled';
    const STATUS_CONTRACT_REVISION = 'contract_revision';
    const STATUS_CONTRACT_FINISHED = 'contract_finished';

    protected $fillable = [
        'client_id',
        'item_id',
        'item_unit_id',
        'status',
        'rental_start_date',
        'rental_end_date',
        'total_price',
        'report',
    ];

    // Cliente relacionado
    public function client()
    {
        return $this->hasOne(Client::class,'id','client_id');
    }

    // Itens relacionados a contratação
    public function item()
    {
        return $this->hasOne(Item::class,'id','item_id');
    }

    // Unidade relacionados a contratação
    public function unit()
    {
        return $this->hasOne(ItemUnit::class,'id','item_unit_id');
    }

    public function log()
    {
        return $this->hasMany(RentalLog::class,'rental_id','id');
    }

    /**
     * Get Status Label
     * Retorna o status da locação
     *
     * Rental::getStatusLabels()[$status]
     *
     * @return string[]
     */
    public static function getStatusLabels()
    {
        return [
            self::STATUS_REQUEST_NEW => 'Nova Requisição',
            self::STATUS_REQUEST_ACCEPTED => 'Aceito',
            self::STATUS_REQUEST_REFUSED => 'Recusado',
            self::STATUS_PREPARATION_ITEM_SEPARATION => 'Separação dos Itens',
            self::STATUS_PREPARATION_CLIENT_IDENTIFICATION => 'Identificação do Cliente',
            self::STATUS_ALLOCATION_ROUTE_PREPARATION => 'Preparação da Rota',
            self::STATUS_ALLOCATION_ON_ROUTE => 'Em Rota de Entrega',
            self::STATUS_ALLOCATION_SUCCESSFUL_INSTALLATION => 'Instalação com Sucesso',
            self::STATUS_ALLOCATION_INSTALLATION_ISSUE => 'Problema durante a Instalação',
            self::STATUS_CONTRACT_ACTIVE => 'Contrato Ativo',
            self::STATUS_CONTRACT_FINISHED => 'Finalizado',
            self::STATUS_CONTRACT_CANCELLED => 'Cancelado',
            self::STATUS_CONTRACT_REVISION => 'Revisão'
        ];
    }


    public static function getStatusStyle()
    {
        return [
            self::STATUS_REQUEST_NEW => 'bg-gray-100 text-gray-800 text-xs font-medium mr-2 px-2.5 py-0.5 rounded dark:bg-gray-700 dark:text-gray-300',
            self::STATUS_REQUEST_ACCEPTED => 'bg-green-100 text-green-800 text-xs font-medium mr-2 px-2.5 py-0.5 rounded dark:bg-green-700 dark:text-green-300',
            self::STATUS_REQUEST_REFUSED => 'bg-red-100 text-red-800 text-xs font-medium mr-2 px-2.5 py-0.5 rounded dark:bg-red-900 dark:text-red-300',
            self::STATUS_PREPARATION_ITEM_SEPARATION => 'bg-blue-100 text-blue-800 text-xs font-medium mr-2 px-2.5 py-0.5 rounded dark:bg-blue-900 dark:text-blue-300',
            self::STATUS_PREPARATION_CLIENT_IDENTIFICATION => 'bg-blue-100 text-blue-800 text-xs font-medium mr-2 px-2.5 py-0.5 rounded dark:bg-blue-900 dark:text-blue-300',
            self::STATUS_ALLOCATION_ROUTE_PREPARATION => 'bg-blue-100 text-blue-800 text-xs font-medium mr-2 px-2.5 py-0.5 rounded dark:bg-blue-900 dark:text-blue-300',
            self::STATUS_ALLOCATION_ON_ROUTE => 'bg-blue-100 text-blue-800 text-xs font-medium mr-2 px-2.5 py-0.5 rounded dark:bg-blue-900 dark:text-blue-300',
            self::STATUS_ALLOCATION_SUCCESSFUL_INSTALLATION => 'bg-green-100 text-green-800 text-xs font-medium mr-2 px-2.5 py-0.5 rounded dark:bg-green-700 dark:text-green-300',
            self::STATUS_ALLOCATION_INSTALLATION_ISSUE => 'bg-red-100 text-red-800 text-xs font-medium mr-2 px-2.5 py-0.5 rounded dark:bg-red-900 dark:text-red-300',
            self::STATUS_CONTRACT_ACTIVE => 'bg-green-100 text-green-800 text-xs font-medium mr-2 px-2.5 py-0.5 rounded dark:bg-green-700 dark:text-green-300',
            self::STATUS_CONTRACT_FINISHED => 'bg-yellow-100 text-yellow-800 text-xs font-medium mr-2 px-2.5 py-0.5 rounded dark:bg-yellow-900 dark:text-yellow-300',
            self::STATUS_CONTRACT_CANCELLED => 'bg-red-100 text-red-800 text-xs font-medium mr-2 px-2.5 py-0.5 rounded dark:bg-red-900 dark:text-red-300',
            self::STATUS_CONTRACT_REVISION => 'bg-yellow-100 text-yellow-800 text-xs font-medium mr-2 px-2.5 py-0.5 rounded dark:bg-yellow-900 dark:text-yellow-300'
        ];
    }

    // Mover ao status "Nova Requisição"
    public function moveToNew()
    {
        $this->status = self::STATUS_REQUEST_NEW;
        $this->save();
    }

    // Mover ao status "Aceito"
    public function moveToAccepted()
    {
        $this->status = self::STATUS_REQUEST_ACCEPTED;
        $this->save();
    }

    // Mover ao status "Recusado", informando o motivo
    public function moveToRefused($reason)
    {
        $this->status = self::STATUS_REQUEST_REFUSED;
        $this->report = $reason;
        $this->save();
    }

    // Move ao status "Separação"
    public function moveToItemSeparation()
    {
        $this->status = self::STATUS_PREPARATION_ITEM_SEPARATION;
        $this->save();
    }

    // Move ao status "Identificação do Cliente"
    public function moveToClientIdentification()
    {
        $this->status = self::STATUS_PREPARATION_CLIENT_IDENTIFICATION;
        $this->save();
    }

    // Move ao status "Preparação da Rota"
    public function moveToRoutePreparation()
    {
        $this->status = self::STATUS_ALLOCATION_ROUTE_PREPARATION;
        $this->save();
    }

    // Move ao status "Em Rota de Entrega"
    public function moveToOnRoute()
    {
        $this->status = self::STATUS_ALLOCATION_ON_ROUTE;
        $this->save();
    }

    // Move ao status "Instalação com Sucesso"
    public function moveToSuccessfulInstallation()
    {
        $this->status = self::STATUS_ALLOCATION_SUCCESSFUL_INSTALLATION;
        $this->save();
    }

    // Move ao status "Problema durante a Instalação" informando o motivo
    public function moveToInstallationIssue($reason)
    {
        $this->status = self::STATUS_ALLOCATION_INSTALLATION_ISSUE;
        $this->report = $reason;  // Considerando que você tenha um campo 'report' para armazenar a razão do problema
        // Lógicas adicionais, se necessário
        $this->save();
    }

    // Move ao status "Contrato Ativo"
    public function moveToContractActive()
    {
        $this->status = self::STATUS_CONTRACT_ACTIVE;
        $this->save();
    }

    // Finaliza o contrato
    public function moveToFinished()
    {
        $this->status = self::STATUS_CONTRACT_FINISHED;
        $this->save();
    }

}
