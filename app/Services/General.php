<?php

namespace App\Services;

use App\Models\Category;
use App\Models\Rental;
use Illuminate\Support\Facades\Auth;

class General
{

    /**
     * Currency Formatter
     * @param $number
     * @return string
     */
    public static function currencyFormatter( $number )
    {
        return number_format($number,2,',','.');
    }


}
