<?php

namespace App\Services;

use App\Models\Cart;
use App\Models\CartItem;
use App\Models\Item;
use App\Models\ItemUnit;
use App\Models\Rental;
use Carbon\Carbon;

/**
 * Class CartService
 * Carrinho de compras para o pedido de locação
 *
 * @package App\Services
 */
class CartService
{
    protected $userId;

    public function __construct()
    {
        $this->userId = auth()->guard('client')->user()->client_id;

        // Em toda a construção do carrinho a quantidade de estoque é verificada
        $this->validateQuantityStock();
    }


    /**
     * Validate Quantity Stock
     */
    private function validateQuantityStock()
    {
        $cart = $this->getCart();
        $cartItems = $cart->items;
        foreach ($cartItems as $cartItem) {
            $item = $cartItem->item;
            if ($cartItem->quantity > $item->availableUnits()) {
                $cartItem->quantity = $item->availableUnits();
                $cartItem->save();
            }
        }
    }

    /**
     * Get Cart
     * Retorna o carrinho do cliente, Geralmente será sempre o mesmo registro pois ele nunca é excluido
     * @return mixed
     */
    public function getCart()
    {
        return Cart::firstOrCreate(['user_id' => $this->userId]);
    }

    /**
     * Get Cart Items
     * Itens do carrinho
     *
     * @return mixed
     */
    public function getCartItems()
    {
        $cart = $this->getCart();
        return $cart->items()->with('item')->get();
    }

    /**
     * Get Item Count
     * Soma a quantidade de itens no carrinho
     *
     * @return mixed
     */
    public function getItemCount()
    {
        $cart = $this->getCart();
        return CartItem::where('cart_id', $cart->id)->sum('quantity');
    }

    /**
     * Get Items Substotal
     * Valor total do carrinho
     *
     * @return string
     */
    public function getItemsSubtotal()
    {
        $cart = $this->getCart();
        $itens = CartItem::where('cart_id', $cart->id)->get();
        $subtotal = collect($itens)->sum(function ( $item ){
            return ($item->item->daily_rental_price * $item->quantity ) * $item->days;
        });
        return number_format($subtotal,2);
    }

    /**
     * Add Item to Cart
     * Adicionar um item no carrinho
     *
     * @param $itemId
     * @param $quantity
     * @return string
     */
    public function addItemToCart($itemId, $quantity)
    {
        $item = Item::find($itemId);

        if(!$item)
            return 'Item não encontrado.';

        if($item->availableUnits() < $quantity)
            return 'Não há estoque suficiente.';

        $cart = $this->getCart();
        $cartItem = $cart->items()->firstOrNew(['item_id' => $itemId]);
        $cartItem->quantity += $quantity;
        $cartItem->save();

        return 'Adicionado com sucesso!';
    }

    public function updateItemDays($cartId, $days)
    {
        $cart = $this->getCart();
        $cartItem = CartItem::where('cart_id', $cart->id)->where('id', $cartId)->first();

        $cartItem->days = $days;
        $cartItem->save();

        return 'Item updated successfully';
    }

    /**
     * Update Item Start Date
     * Configuração da data de instalação do equipamento
     * - é Realizado a atualização do "end_date" com base nos dias escolhidas para o equipamento
     *
     * @param $cartId
     * @param $startDate
     * @return string
     */
    public function updateItemStartDate($cartId, $startDate)
    {
        $cart = $this->getCart();
        $cartItem = CartItem::where('cart_id', $cart->id)->where('id', $cartId)->first();

        if($startDate==null){
            $cartItem->start_date = null;
            $cartItem->end_date = null;
        }else{
            $cartItem->start_date = $startDate;
            $cartItem->end_date = Carbon::parse($startDate)->addDays( $cartItem->days )->format('Y-m-d');
        }

        $cartItem->save();

        return 'Atualizado com sucesso.';
    }

    /**
     * Update Item Quantity
     * Atualiza a quantide de equipamentos,
     * - Verifica se há quantidades disponíveis para locação
     *
     * @param $itemId
     * @param $quantity
     * @return array|\Illuminate\Http\JsonResponse
     */
    public function updateItemQuantity($itemId, $quantity)
    {

        // Quando há uma interação diferente na resposta do carrinho
        $message = null;

        $item = Item::find($itemId);

        if(!$item) {
            return response(500)->json([
                'message' => 'Item não encontrado.',
                'quantityUpdated' => 0
            ]);
        }

        if( ($item->availableUnits() ) <= $quantity) {
            $message = 'Não há unidades disponíveis.';
            $quantity = $item->availableUnits() ;
        }

        $cart = $this->getCart();
        $cartItem = CartItem::where('cart_id', $cart->id)->where('item_id', $itemId)->first();

        $cartItem->quantity = $quantity;
        $cartItem->save();

        return [
            'message' => !$message ? 'Atualizado com sucesso.' : $message ,
            'quantityUpdated' => $quantity
        ];
    }


    /**
     * Remove Item From Cart
     * Remove um item do carrinho
     *
     * @param $cartItemId
     * @return bool
     */
    public function removeItemFromCart($cartItemId)
    {
        $cartItem = CartItem::find($cartItemId);
        if($cartItem) {
            $cartItem->delete();
            return true;
        }
        return false;
    }

    /**
     * Converter Cart In Orders
     * Criação das locações
     * Excluir itens do carrinho
     *
     * @return array
     * @throws \Exception
     */
    public function converterCartIdOrders()
    {

        $orders = [];

        $cart = $this->getCart();
        $cartItem = $cart->items();

        foreach ($cartItem->get() as $cartItem){

            for( $requestQuantity = 1; $requestQuantity <= $cartItem['quantity']; $requestQuantity ++ ){

                // Busca um item em estoque
                $itemUnit =  $cartItem->item->itemUnits()->where('status', ItemUnit::STATUS_AVAILABLE)->inRandomOrder()->first();
                if (!$itemUnit) {
                    throw new \Exception('Aconteceu um erro ao executar a locação');
                }

                // Atualiza o status para reservado
                $itemUnit->update(['status' => ItemUnit::STATUS_RESERVED]);

                // Vencimento
                $startDate = Carbon::parse($cartItem->start_date);
                $endDate = Carbon::parse($cartItem->end_date);

                $orders[] = Rental::create([
                    'client_id' => $cart->user_id,
                    'item_id' => $cartItem->item_id,
                    'item_unit_id' => $itemUnit->id,
                    'rental_start_date' => $startDate->format('Y-m-d'),
                    'rental_end_date' => $endDate->format('Y-m-d'),
                    'total_price' => $cartItem->item->daily_rental_price * ($endDate->diff($startDate)->days + 8)
                ]);

            }

            $cartItem->delete();    // Limpa o carrinho do cliente

        }

        return $orders;

    }
}
