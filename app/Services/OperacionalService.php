<?php

namespace App\Services;

use App\Actions\UpdateRentalStatus;
use App\Models\Rental;
use Illuminate\Pagination\LengthAwarePaginator;

/**
 * Class OperacionalService
 * Fornece métodos para navegar nas locações e atualizar status
 *
 * @package App\Services
 */
class OperacionalService {


    // Status para operar novos itens
    const PENDING_STATUS = [
        Rental::STATUS_REQUEST_ACCEPTED,
        Rental::STATUS_REQUEST_REFUSED,
    ];

    // Status para operar a preparação
    const PREPARATION_STATUS = [
        Rental::STATUS_REQUEST_ACCEPTED,
        Rental::STATUS_PREPARATION_ITEM_SEPARATION,
        Rental::STATUS_PREPARATION_CLIENT_IDENTIFICATION,
        Rental::STATUS_ALLOCATION_ROUTE_PREPARATION,
    ];

    // Status para operar quando estiver em configuração
    const SETUP_STATUS = [
        Rental::STATUS_ALLOCATION_ROUTE_PREPARATION,
        Rental::STATUS_ALLOCATION_ON_ROUTE,
        Rental::STATUS_ALLOCATION_SUCCESSFUL_INSTALLATION,
        Rental::STATUS_ALLOCATION_INSTALLATION_ISSUE,
    ];

    // Status para operar quando o contrato estiver ativo
    const ACTIVED_STATUS = [
        Rental::STATUS_ALLOCATION_SUCCESSFUL_INSTALLATION,
        Rental::STATUS_CONTRACT_ACTIVE,
        Rental::STATUS_CONTRACT_REVISION,
        Rental::STATUS_CONTRACT_CANCELLED,
        Rental::STATUS_CONTRACT_FINISHED,
        Rental::STATUS_REQUEST_REFUSED,
    ];

    /**
     * Get View Mode Statuses
     * Com base no parametro Viewmode, retorna os status usados na query
     *
     * @param $viewMode
     * @return false|string[]
     */
    protected function getViewModeStatuses( $viewMode )
    {
        switch ($viewMode) {
            case 'all':
                break;
            case 'processing':
                return [
                    'request_new',
                    'request_accepted',
                    'preparation_item_separation',
                    'preparation_client_identification',
                    'allocation_route_preparation',
                    'allocation_on_route',
                    'allocation_successful_installation',
                    'allocation_installation_issue'
                ];
                break;
            case 'active':
                return  [
                    'contract_active',
                    'contract_revision'
                ];
                break;
            case 'closed':
                return  [
                    'contract_finished',
                    'request_refused'
                ];
                break;
        }

        return false;

    }


    /**
     * Get Rentals
     *
     * @param string $viewMode
     * @param string|null $rentalId
     * @param string|null $status
     * @return \Illuminate\Contracts\Pagination\LengthAwarePaginator
     */
    public function getRentals( $viewMode, $rentalId = null, $status = null)
    {
        // Query
        $rentals = Rental::query();

        // Relações
        $rentals->with('item.category', 'unit', 'client');

        // Busca pelo ID ( Simples )
        if ($rentalId) {
            $rentals->where('id', 'like', $rentalId . '%');
        }

        // Busca status no ViewMode
        $statuses = $this->getViewModeStatuses( $viewMode );
        if($statuses)
            $rentals->whereIn('status',$statuses);

        // Coluna status
        if ($status)
            $rentals->where('status', $status);

        return $rentals->orderBy('id', 'desc')->paginate(10);

    }

    /**
     * Get Avaliable Status
     * @param $viewMode
     * @return mixed[]
     */
    public function getAvailableStatus( $viewMode )
    {

        // Início da query
        $rentalQuery = Rental::query();
        $rentalQuery = $rentalQuery->select('status');

        // Busca status no ViewMode
        $statuses = $this->getViewModeStatuses( $viewMode );
        if($statuses)
            $rentalQuery->whereIn('status',$statuses);

        $statuses = $rentalQuery ->get()
            ->pluck('status')
            ->unique()
            ->toArray();

        return $statuses;
    }

    public function getRentalWithDetails(int $rentalId)
    {
        $rental = Rental::where('id', $rentalId)
            ->with(['client', 'item', 'unit', 'log'])
            ->firstOrFail();

        $status = [];

        // 1 Stage
        if (in_array($rental->status, ['request_new'])) {
            $status = self::PENDING_STATUS;
        }

        // 2 Stage
        if (in_array($rental->status, array_merge(['request_accepted'], self::PREPARATION_STATUS))) {
            $status = self::PREPARATION_STATUS;
        }

        // 3 Stage
        if (in_array($rental->status, self::SETUP_STATUS)) {
            $status = self::SETUP_STATUS;
        }

        // 4 Stage
        if ($rental->status == "allocation_route_preparation" ||
            $rental->status == "allocation_successful_installation" ||
            $rental->status == "contract_revision" ||
            $rental->status == "contract_cancelled") {
            $status = self::ACTIVED_STATUS;
        }

        // 5 Stage
        if ($rental->status == "request_refused") {
            $status = [];
        }

        return [
            'status' => $status,
            'rental' => $rental,
        ];
    }

    /**
     * Update Rental Status
     *
     * @param int $rentalId
     * @param int $operatorId
     * @param string $rentalStatus
     * @param string $reasonDescription
     * @return array
     */
    public function updateRentalStatus(int $rentalId, int $operatorId, string $rentalStatus, string $reasonDescription) {

        try {
            $result = UpdateRentalStatus::run($rentalId, $operatorId, $rentalStatus, $reasonDescription);

            if( $result ) {
                return [
                    'success' => true,
                    'message' => 'Registro foi atualizado'
                ];
            }else{
                return [
                    'success' => false,
                    'message' => 'Não há mudanças a serem realizadas'
                ];
            }

        } catch ( \Exception $exception ) {
            return [
                'success' => false,
                'message' => $exception->getMessage()
            ];
        }

    }

}
