<?php

namespace App\Services;

use App\Models\Category;
use App\Models\Item;
use App\Models\Rental;
use Illuminate\Support\Facades\Auth;

class ItemService
{

    /**
     * Get Avaliable Items By Category
     * Retorna todos os itens agrupando por categoria
     *
     * @return array
     */
    public function getAvaliableItemsByCategory(): array
    {

        $data = [];

        // Árvore de categorias
        $categories = Category::all();

        foreach ($categories as $category) {
            $count = Rental::whereHas('item', function ($query) use ($category) {
                $query->where('category_id', $category->id)
                    ->with('availableUnits');
            })
                ->with('item')
                ->with('item.category')
                ->get();

            $data[$category->name] = $count;
        }

        return $data;

    }

    public function getItemsByCategory( $categoryId ){

        $category = Category::where('id', $categoryId)->first();

        $Items = Item::where('category_id', $categoryId)
            ->with('category')
            ->get();

        return [
            'category' => $category,
            'itens' => $Items
        ];
    }

    /**
     * Get active items in rentals by category.
     *
     * @return array
     */
    public function getActiveItemsByCategory(): array
    {

        $data = [];

        $categories = Category::all();

        foreach ($categories as $category) {
            $count = Rental::whereHas('item', function ($query) use ($category) {
                $query->where('category_id', $category->id);
            })
                ->where('client_id', Auth::guard('client')->user()->client_id )
                ->whereNotIn('status',[
                    Rental::STATUS_CONTRACT_FINISHED,
                    Rental::STATUS_REQUEST_REFUSED,
                ])
                ->count();

            $data[$category->name] = $count;
        }

        return $data;
    }
}
