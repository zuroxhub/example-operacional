<?php

namespace App\Actions;

use App\Models\Rental;
use App\Models\RentalLog;
use Carbon\Carbon;
use Lorisleiva\Actions\Concerns\AsAction;

/**
 * Class UpdateRentalStatus
 * Esta action tem como objetivo atualizar o status de uma locação, incrementando o histórico de modificações
 *
 * @package App\Actions
 */
class UpdateRentalStatus
{
    use AsAction;


    /**
     * @param $rentalId
     * @param $operatorId
     * @param $status
     * @param $reasonDescription
     * @return bool
     * @throws \Exception
     */
    public function handle( $rentalId, $operatorId, $status, $reasonDescription )
    {

        // Busca a locação
        $rentalEntity = Rental::where('id',$rentalId)->first();
        if(!isset($rentalEntity->id)) throw new \Exception('Locação não encontrada');

        $payload = [
            'rental_id' => $rentalId,
            'operator_id' => $operatorId,
            'status' => $status,
            'description' => $reasonDescription,
            'created_at' => Carbon::now(),
        ];

        // Status de atualização igual ao do banco de dados
        // Registro sem descrição
        if( $rentalEntity->status == $status && $reasonDescription=='N/A'){

            // Nope!
            return false;
        }

        RentalLog::create($payload);

        $rentalEntity->status = $status;
        $rentalEntity->save();

        return true;
    }
}
