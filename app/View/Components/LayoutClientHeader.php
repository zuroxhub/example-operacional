<?php

namespace App\View\Components;

use Illuminate\View\Component;
use Illuminate\View\View;

class LayoutClientHeader extends Component
{

    public $menu;

    public function __construct( $menu )
    {
        $this->menu = $menu;
    }

    /**
     * Get the view / contents that represents the component.
     */
    public function render(): View
    {
        return view('client.layouts.partials.header');
    }
}
