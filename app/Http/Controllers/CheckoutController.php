<?php

namespace App\Http\Controllers;

use App\Models\Cart;
use App\Models\CartItem;
use App\Services\CartService;
use Illuminate\Http\Request;

/**
 * Class CheckoutController
 * Processo de conversão do carrinho em locações
 *
 * @package App\Http\Controllers
 */
class CheckoutController extends Controller
{

    /**
     * Checkout Index
     * Configuração dos dias e data de instação dos equipamentos
     *
     * @param CartService $cartService
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View|\Illuminate\Foundation\Application
     */
    public function index(CartService $cartService)
    {
        $cart = $cartService->getCart();
        $items = $cartService->getCartItems();

        return view('client.checkout.index', compact('cart', 'items'));
    }

    /**
     * Checkout Update
     * Receber as atualizações dos dias e datas de instação
     *
     * @param Request $request
     * @param CartService $CartService
     * @return \Illuminate\Http\RedirectResponse
     */
    public function checkoutUpdate( Request  $request, CartService $CartService)
    {
        $index = array_keys( $request->get('days') );

        $days = $request->get('days');
        $date = $request->get('date');

        foreach ( $index as $ponteiro ){

            $iDays = @$days[ $ponteiro ];
            $iDate = @$date[ $ponteiro ];

            if($iDays && is_numeric($iDays))
                $CartService->updateItemDays( $ponteiro, $iDays);

            if($iDate){
                $CartService->updateItemStartDate( $ponteiro, $iDate);
            }
        }

        return redirect()->back();
    }

    /**
     * Clean Item Date
     * Remove uma data de instalação
     *
     * @param $itemId
     * @param CartService $CartService
     * @return \Illuminate\Http\RedirectResponse
     */
    public function cleanItemDate ( $itemId , CartService $CartService) {

        $CartService->getCart();
        $CartService->updateItemStartDate( $itemId, null);
        return redirect()->back();
    }

    /**
     * Checkout Action
     * Tela para confirmação do pedido
     *
     * @param Request $request
     * @param CartService $cartService
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View|\Illuminate\Foundation\Application
     */
    public function checkoutAction( Request  $request, CartService $cartService )
    {
        $cart = $cartService->getCart();
        $items = $cartService->getCartItems();

        return view('client.checkout.confirmation', compact('cart', 'items'));
    }

    /**
     * Checkout Accepted
     * Processamento das locações
     *
     * @param CartService $cartService
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Foundation\Application|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     * @throws \Exception
     */
    public function checkoutAccepted ( CartService $cartService ){

        $cart = $cartService->getCart();
        $items = $cartService->getCartItems();

        // Cria as locações
        $createdOrders = $cartService->converterCartIdOrders();

        return redirect(route('client.rentals'));

    }

}
