<?php

namespace App\Http\Controllers;

use App\Models\Cart;
use App\Models\CartItem;
use App\Services\CartService;
use Illuminate\Http\Request;

/**
 * Class CartController
 * Preparação dos itens para locação
 *
 * @package App\Http\Controllers
 */
class CartController extends Controller
{

    /**
     * Index
     * Carrinho de compras
     *
     * @param CartService $cartService
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View|\Illuminate\Foundation\Application
     */
    public function index(CartService $cartService)
    {
        $cart = $cartService->getCart();
        $items = $cartService->getCartItems();

        return view('client.cart.index', compact('cart', 'items'));
    }

    /**
     * Store
     * Adicionar um item no Carrinho
     *
     * @param Request $request
     * @param CartService $cartService
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(Request $request, CartService $cartService)
    {
        $request->validate([
            'item_id' => 'required|exists:items,id',
            'quantity' => 'required|integer|min:1',
        ]);

        $cartService->addItemToCart($request->item_id, $request->quantity);

        return response()->json([
            'message' => 'success',
        ]);
    }

    /**
     * Destroy
     * Excluir um item do carrinho
     *
     * @param CartItem $cartItem
     * @param CartService $cartService
     * @return \Illuminate\Http\RedirectResponse
     */
    public function destroy(CartItem $cartItem, CartService $cartService)
    {
        if($cartService->removeItemFromCart($cartItem->id)) {
            return redirect()->route('cart.index');
        }
        return redirect()->route('cart.index');
    }


    /**
     * Ajax Total
     * Retorna o total e subtotal do carrinho para atualização de UI
     *
     * @param CartService $cartService
     * @return array
     */
    public function ajaxTotal( CartService  $cartService ){

        // Total de itens no carrinho
        $itens = $cartService->getItemCount();

        // Subtotal dos itens
        $subtotal = $cartService->getItemsSubtotal();

        return [
            'total' => $itens,
            'subtotal' => $subtotal,
        ];

    }

    /**
     * Ajax Update
     * Recebe via ajax a atualização de quantidades de um equipamento do carrinho
     *
     * @param CartService $cartService
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function ajaxUpdate( CartService $cartService, Request $request )
    {
        $itemId = $request->input('item_id');
        $quantity = $request->input('quantity');

        $response = $cartService->updateItemQuantity($itemId, $quantity);

        return response()->json([
            'message' => $response
        ]);
    }

}
