<?php

namespace App\Http\Controllers;

use App\Models\Rental;
use App\Services\ItemService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class ClientDashboardController extends Controller
{

    public function dashboard( ItemService $ItemService )
    {

        // Usuário autenticado
        $client = Auth::guard('client')->user();

        // Obtem o total de itens ativos em locação agrupando por categoria
        $itensActiveByCategory = $ItemService->getActiveItemsByCategory();

        echo view('client.dashboard',[
            'businessClient' => $client->businessClient,
            'itensActive' => $itensActiveByCategory
        ]);
    }


}
