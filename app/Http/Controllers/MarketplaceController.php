<?php

namespace App\Http\Controllers;

use App\Models\Category;
use App\Models\Item;
use App\Services\ItemService;
use Illuminate\Http\Request;

/**
 * Class MarketplaceController
 * Navegação no catálogo de produtos
 *
 * @package App\Http\Controllers
 */
class MarketplaceController extends Controller
{

    /**
     * Navegação Principal
     *
     * @param ItemService $ItemService
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View|\Illuminate\Foundation\Application
     * @throws \Psr\Container\ContainerExceptionInterface
     * @throws \Psr\Container\NotFoundExceptionInterface
     */
    public function navigation( ItemService  $ItemService)
    {
        // Categoria atual
        $categoryId = ( request()->has('categoryId') && is_numeric(request()->get('categoryId')) ) ? (int) request()->get('categoryId') : 'all';

        $categories = Category::get();

        // Navegando em todas as categorias
        if( $categoryId == 'all') {

            // Obtem todos os itens agrupados por categoria
            $avaliableItems = $ItemService->getAvaliableItemsByCategory();
            return view('client.marketplace',compact('categories','avaliableItems'));
        }else{

            // Obtem somente os itens da categoria
            $avaliableItems = $ItemService->getItemsByCategory( $categoryId );
            return view('client.marketplace',compact('categories','avaliableItems'));
        }

    }

    /**
     * Ajax Detail Item
     * Carrega a view com os detalhes do item para o Drawer
     *
     * @param $id
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View|\Illuminate\Foundation\Application
     */
    public function ajaxDetailItem( $id ){

        $item = Item::where('id', $id)->first();
        return view('client.marketplace.partials.modal_item', compact('item'));
    }

}
