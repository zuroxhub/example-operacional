<?php

namespace App\Http\Controllers;

use App\Actions\UpdateRentalStatus;
use App\Models\Rental;
use App\Services\OperacionalService;
use Illuminate\Http\Request;

class OperacionalController extends Controller
{

    // Ponteiro para o service operacional
    protected $service;

    /**
     * Constructor.
     *
     * @param OperacionalService $service
     */
    public function __construct(OperacionalService $service) {
        $this->service = $service;
    }


    /**
     *
     * Navigation
     * Rota de navegação entre as locações do sistema
     *
     * @param Request $request
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View|\Illuminate\Foundation\Application
     */
    public function navigation( Request  $request ){

        // Params
        $viewMode = $request->get('viewMode');
        $rentalId = $request->get('rentalId');
        $status = $request->get('status');

        // Detalhes
        $rentals = $this->service->getRentals($viewMode, $rentalId, $status);

        // Status disponíveis na página
        $avaliableStatus = $this->service->getAvailableStatus( $viewMode );

        return view('admin.operacional', compact('rentals', 'avaliableStatus'));

    }

    /**
     * Rental Details
     * Carrega o modal(Drawer) com o conteúdo da locação para atualização
     *
     * @param $rentalId
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View|\Illuminate\Foundation\Application
     */
    public function rentalDetails($rentalId) {

        $rentDetails = $this->service->getRentalWithDetails($rentalId);

        $status = $rentDetails['status'];
        $rental = $rentDetails['rental'];

        return view('admin.operacional.modal_rental', compact('status', 'rental'));
    }


    /**
     * @param $rentalId
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     * Rental Update
     * Recebe via POST  os parametros:
     * - operatorId
     * - rentalStatus
     * - operationDescription (opcional)
     */
    public function rentalUpdate($rentalId, Request $request) {

        $operatorId = $request->get('operatorId');
        $rentalStatus = $request->get('rentalStatus');
        $reasonDescription = $request->get('operationDescription') ?: 'N/A';

        $result = $this->service->updateRentalStatus(  $rentalId, $operatorId, $rentalStatus, $reasonDescription );

        return response()->json($result);

    }

}
