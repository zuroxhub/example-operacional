<?php

namespace App\Http\Controllers;

use App\Models\Rental;
use App\Services\ItemService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class RentalsController extends Controller
{

    public function rentals( Request $request ){

        // Modo de visualiação da página
        $viewMode = $request->get('viewMode') ?: null;

        // Customer Autenticado
        $client = Auth::guard('client')->user();

        // Início da query
        $rentals = Rental::query();
        $rentals = $rentals->where('client_id',$client->client_id);
        $rentals = $rentals->with('item.category');

        // Modo de visão
        if($viewMode && $viewMode!='all'){
            if( $viewMode == 'processing'){
                $rentals = $rentals->whereIn('status',[
                    Rental::STATUS_REQUEST_NEW,
                    Rental::STATUS_REQUEST_ACCEPTED,
                    Rental::STATUS_PREPARATION_ITEM_SEPARATION,
                    Rental::STATUS_PREPARATION_CLIENT_IDENTIFICATION,
                    Rental::STATUS_ALLOCATION_ROUTE_PREPARATION,
                    Rental::STATUS_ALLOCATION_ON_ROUTE,
                    Rental::STATUS_ALLOCATION_SUCCESSFUL_INSTALLATION,
                    Rental::STATUS_ALLOCATION_INSTALLATION_ISSUE,
                ]);
            }
            if( $viewMode == 'active'){
                $rentals = $rentals->whereIn('status',[
                    Rental::STATUS_CONTRACT_ACTIVE,
                    Rental::STATUS_CONTRACT_REVISION,
                ]);
            }
            if( $viewMode == 'closed'){
                $rentals = $rentals->whereIn('status',[
                    Rental::STATUS_CONTRACT_FINISHED,
                    Rental::STATUS_CONTRACT_CANCELLED,
                    Rental::STATUS_REQUEST_REFUSED,
                ]);
            }
        }

        // Ordem & Paginação
        $rentals = $rentals->orderBy('id','desc');
        $rentals = $rentals->paginate(30);

        return view('client.rentals',compact('rentals'));

    }

    public function details( $id ){

        $client = Auth::guard('client')->user();
        $rental = Rental::query();
        $rental = $rental->where('client_id',$client->client_id);
        $rental = $rental->where('id',$id);

        $rental = $rental->with('client');
        $rental = $rental->with('item');
        $rental = $rental->with('unit');

        $rental = $rental->firstOrFail();

        return view('client.rentail.details',compact('rental'));

    }

}
